package Vista;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import Controlador.ColorString;
import Controlador.SubirController;
import Idioma.ControlIdioma;
import Modelo.Album;
import Modelo.AlbumMetodos;
import Modelo.Categoria;
import Modelo.CategoriaMetodos;
import Modelo.Usuario;
import Temas.ControlTemas;
import java.awt.Color;


public class Subir extends JFrame {

	private JPanel contentPane;
	private JTextField txtCancion;
	ControlTemas style = ControlTemas.getControlTemas();
	ColorString col = new ColorString();
	SubirController sc = null;
	ArrayList<Album> albumes = null;
	ArrayList<Categoria> categorias = null;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public Subir() {
		sc = new SubirController();

		setIconImage(Toolkit.getDefaultToolkit().getImage(Subir.class.getResource("/Imagenes/logoEsquinaBuena.png")));
		setTitle("Coffee Sound - Subir Cancion");


		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 418, 305);
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelSuperior = new JPanel();
		panelSuperior.setBackground(col.getColor(style.getProperty("FPrincipal")));
		contentPane.add(panelSuperior, BorderLayout.NORTH);
		
		JLabel lblSubirCancion = new JLabel("Subir Cancion");
		panelSuperior.add(lblSubirCancion);
		lblSubirCancion.setForeground(col.getColor(style.getProperty("CPrincipal")));
		lblSubirCancion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		DefaultComboBoxModel dcb = new DefaultComboBoxModel();
		albumes=AlbumMetodos.getAlbumesUser(Usuario.getCurrentUser());
		
		for(Album album : albumes){
			dcb.addElement(album.getNombre());
		}
		
		dcb.addElement(ControlIdioma.getControlIdioma().getProperty("AnadirAlbum"));
		
		categorias = CategoriaMetodos.getCategorias();
		DefaultComboBoxModel dcbm = new DefaultComboBoxModel();
		for(Categoria categoria : categorias){
			dcbm.addElement(categoria.getNombre());
		}
		
		JPanel panel = new JPanel();
		panel.setBackground(col.getColor(style.getProperty("FSecundario")));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_Superior = new JPanel();
		panel_Superior.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel.add(panel_Superior);
		
		JLabel lblSelecciona = new JLabel(ControlIdioma.getControlIdioma().getProperty("Seleccionar"));
		lblSelecciona.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_Superior.add(lblSelecciona);
		
		JButton btnSeleccionar = new JButton(ControlIdioma.getControlIdioma().getProperty("Seleccionar"));
		panel_Superior.add(btnSeleccionar);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel.add(panel_1);
		
		JLabel lblAlbum = new JLabel("Album");
		lblAlbum.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_1.add(lblAlbum);
		
		JComboBox comboBoxAlbum = new JComboBox();
		comboBoxAlbum.setModel(dcb);
		
		comboBoxAlbum.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(comboBoxAlbum.getSelectedItem().toString().equals(ControlIdioma.getControlIdioma().getProperty("AnadirAlbum"))){
					CreadorAlbum ca = new CreadorAlbum();
					ca.setVisible(true);
				}
				
			}
		});
		panel_1.add(comboBoxAlbum);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel.add(panel_2);
		
		JLabel lblCancion = new JLabel("Cancion");
		lblCancion.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_2.add(lblCancion);
		
		txtCancion = new JTextField();
		panel_2.add(txtCancion);
		txtCancion.setColumns(10);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel.add(panel_4);
		
		JLabel lblCategoria = new JLabel(ControlIdioma.getControlIdioma().getProperty("Categoria"));
		lblCategoria.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_4.add(lblCategoria);
		JComboBox comboBoxCategoria = new JComboBox(dcbm);
		panel_4.add(comboBoxCategoria);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(col.getColor(style.getProperty("FPrincipal")));
		contentPane.add(panel_5, BorderLayout.SOUTH);
		
		JButton btnCancelar = new JButton(ControlIdioma.getControlIdioma().getProperty("Cancelar"));
		panel_5.add(btnCancelar);
		
		JButton btnOk = new JButton("OK");
		panel_5.add(btnOk);
		
		sc.setActionCancelar(btnCancelar, this);
		sc.setActionSeleccionar(btnSeleccionar);
		sc.setActionSubir(btnOk,txtCancion,comboBoxAlbum,comboBoxCategoria,albumes,categorias);

	}

}
