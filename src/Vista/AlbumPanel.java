package Vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controlador.AlbumPanelController;
import Modelo.Album;
import Modelo.UsuarioMetodos;

public class AlbumPanel extends JPanel {
	AlbumPanelController controller = null;
	/**
	 * Create the panel.
	 */
	public AlbumPanel(ArrayList<Album> albumes, JPanel panelcentral) {
		controller = new AlbumPanelController();
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(20);
		flowLayout.setAlignOnBaseline(true);
		this.setMaximumSize(panelcentral.getSize());
		flowLayout.setHgap(20);
		flowLayout.setAlignment(FlowLayout.LEFT);
		for(Album album: albumes){
			JPanel userpanel = new JPanel();
			userpanel.setMinimumSize(new Dimension(90,90));
			userpanel.setMaximumSize(new Dimension(90,90));
			userpanel.setPreferredSize(new Dimension(90,90));
			userpanel.setBackground(Color.DARK_GRAY);
			userpanel.setLayout(new BoxLayout(userpanel, BoxLayout.Y_AXIS));
			JLabel profile = new JLabel(UsuarioMetodos.getUsuario(album.getIduser()).getUsername());
			profile.setAlignmentX(Component.CENTER_ALIGNMENT);
			profile.setForeground(Color.WHITE);
			userpanel.add(profile);
			JButton userbutton = new JButton(album.getNombre());
			
			userbutton.setAlignmentX(Component.CENTER_ALIGNMENT);
			userbutton.setForeground(Color.WHITE);
			userbutton.setBackground(Color.BLACK);
			userbutton.setPreferredSize(new Dimension(90,20));
			userbutton.setMaximumSize(new Dimension(90,20));
			userbutton.setMinimumSize(new Dimension(90,20));
			userpanel.add(userbutton);
			Calendar cal = Calendar.getInstance();
			cal.setTime(album.getFecha());
			JLabel date = new JLabel(""+cal.get(Calendar.DAY_OF_MONTH)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.YEAR));
			date.setAlignmentX(Component.CENTER_ALIGNMENT);
			date.setForeground(Color.WHITE);
			userpanel.add(date);
			this.add(userpanel);
			controller.setActionButton(userbutton, panelcentral, album);;
		}
	}
	

}
