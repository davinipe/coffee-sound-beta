package Vista;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.AbstractListModel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import Confg.Preferencias;
import Controlador.CoffeeSoundController;
import Controlador.ColorString;
import Idioma.ControlIdioma;

import Modelo.Usuario;

import Modelo.UsuarioMetodos;

import Objetos.FTPConnection;
import Temas.ControlTemas;

import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;


public class CoffeeSound extends JFrame{

		//Variables de los elementos
		public JFrame frame;
	
		//Menu
		JMenuBar menuBar;
		JMenu mnArchivo;
		JMenuItem smnAñadirMusica;
		JMenu mnEditar;
		JMenu mnControles;
		JMenuItem smnReproducir;
		JMenuItem smnSiguiente;
		JMenuItem smnAnterior;
		JMenuItem smnAleatoria;
		JMenuItem smnRepetir;
		JMenuItem smnSubirVolumen;
		JMenuItem smnBajarVolumen;
		JMenu mnVer;
		
		Preferencias conf = new Preferencias();
		ControlIdioma lang = ControlIdioma.getControlIdioma();
		ControlTemas style = ControlTemas.getControlTemas();
		ColorString col = new ColorString();
				
		CoffeeSoundController controller = null;
		public static JList listaMusic = new JList();
		public static JPanel panelColaReproduccion;
		public static JPanel panelAutor;
	
	public CoffeeSound() {
		initialize();
	}


	private void initialize() {
		/*
		 * Creamos el frame y le asignamos tanto la dimensiÃ³n mÃ¡xima como la mÃ­nima
		 */
		
		controller = new CoffeeSoundController();
		listaMusic.setMinimumSize(new Dimension(150,900));
		frame = this;
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(CoffeeSound.class.getResource("/Imagenes/logoEsquinaBuena.png")));
		frame.setTitle("Coffee Sound");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		frame.setMaximumSize(new Dimension(width,height));
		frame.setMinimumSize(new Dimension(700,500));
		frame.setSize(new Dimension(1500,1500));
		frame.pack();
	    frame.setLocationRelativeTo(null);
		frame.getContentPane().setBackground(Color.GRAY);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBackground(Color.DARK_GRAY);
		frame.getContentPane().add(panel, BorderLayout.WEST);
		/*
		 * Ejemplo de botÃ³n personalizado
		 */
		JPanel panelIzquierdo = new JPanel();
		panelIzquierdo.setBorder(null);
		panelIzquierdo.setPreferredSize(new Dimension(142,75));
		panelIzquierdo.setBackground(col.getColor(style.getProperty("FLaterales")));
		frame.getContentPane().add(panelIzquierdo, BorderLayout.WEST);
		
		JPanel panelDerecha = new JPanel();
		panelDerecha.setBorder(null);
		panelDerecha.setBackground(col.getColor(style.getProperty("FLaterales")));
		panelDerecha.setPreferredSize(new Dimension(150, 42));
		frame.getContentPane().add(panelDerecha, BorderLayout.EAST);
		panelDerecha.setLayout(new BoxLayout(panelDerecha, BoxLayout.Y_AXIS));
		
		panelAutor = new JPanel();
		
		panelAutor.setBorder(new EmptyBorder(10, 10, 10, 10));
		panelAutor.setBackground(col.getColor(style.getProperty("FAutor")));
		panelDerecha.add(panelAutor);
		
		
		Component verticalStrut_6 = Box.createVerticalStrut(20);
		panelDerecha.add(verticalStrut_6);
		
		
		
		panelColaReproduccion = new JPanel();
		panelColaReproduccion.setBackground(col.getColor(style.getProperty("FColaRep")));
		panelDerecha.add(panelColaReproduccion);
		panelColaReproduccion.setLayout(new BoxLayout(panelColaReproduccion, BoxLayout.Y_AXIS));
		
		JLabel lblNewLabel = new JLabel("Cola de reproduccion");
		//panelColaReproduccion.add(lblNewLabel);
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setForeground(col.getColor(style.getProperty("CColaRep")));
		
		JScrollPane scrollPane = new JScrollPane();
		//panelColaReproduccion.add(scrollPane);
		scrollPane.setViewportView(CoffeeSound.listaMusic);
		/*
		 * Ejemplo de botÃ³n personalizado
		 */
		panelIzquierdo.setLayout(new BoxLayout(panelIzquierdo, BoxLayout.Y_AXIS));
		panel.setLayout(new BorderLayout(0, 0));
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		Component verticalStrut_4 = Box.createVerticalStrut(10);
		panelIzquierdo.add(verticalStrut_4);
		
		JLabel lblPopular = new JLabel(lang.getProperty("Explorar"));
		lblPopular.setForeground(col.getColor(style.getProperty("CExplorar")));
		lblPopular.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPopular.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelIzquierdo.add(lblPopular);
		
		Component verticalStrut_3 = Box.createVerticalStrut(10);
		panelIzquierdo.add(verticalStrut_3);
		
		JList list = new JList();
		list.setBorder(new EmptyBorder(5, 5, 5, 5));
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setFont(new Font("Tahoma", Font.PLAIN, 12));
		list.setBackground(col.getColor(style.getProperty("FExplorar")));
		list.setForeground(col.getColor(style.getProperty("CListaExplorar")));
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {lang.getProperty("NuevasCanciones"), lang.getProperty("NuevosAlbumes"), lang.getProperty("NuevosArtistas")};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		panelIzquierdo.add(list);
		
		Component verticalStrut_5 = Box.createVerticalStrut(20);
		panelIzquierdo.add(verticalStrut_5);
		
		JLabel lblTuMusica = new JLabel(lang.getProperty("TuMusica"));
		lblTuMusica.setForeground(col.getColor(style.getProperty("CTuMusica")));
		lblTuMusica.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTuMusica.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelIzquierdo.add(lblTuMusica);
		
		JList list_1 = new JList();
		list_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		list_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list_1.setForeground(col.getColor(style.getProperty("CListaTuMusica")));
		list_1.setBackground(col.getColor(style.getProperty("FTuMusica")));
		list_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		list_1.setModel(new AbstractListModel() {
			String[] values = new String[] {(lang.getProperty("Mis") + " " + lang.getProperty("Canciones")), (lang.getProperty("Mis") + " " + lang.getProperty("Albumes")) };
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		panelIzquierdo.add(list_1);
		
		Component horizontalStrut = Box.createHorizontalStrut(100);
		panelIzquierdo.add(horizontalStrut);
		
		JPanel panelInferior = new JPanel();
		panelInferior.setBackground(col.getColor(style.getProperty("FMusicaRep")));
		frame.getContentPane().add(panelInferior, BorderLayout.SOUTH);
		panelInferior.setLayout(new BoxLayout(panelInferior, BoxLayout.Y_AXIS));
		
		Component verticalStrut = Box.createVerticalStrut(10);
		panelInferior.add(verticalStrut);
		
		JPanel panelDuracion = new JPanel();
		panelDuracion.setBackground(col.getColor(style.getProperty("FMusicaRep")));
		panelInferior.add(panelDuracion);
		panelDuracion.setLayout(new BoxLayout(panelDuracion, BoxLayout.X_AXIS));
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panelDuracion.add(horizontalStrut_2);
		
		JLabel lblSoundBar = new JLabel("0:0");
		lblSoundBar.setFont(new Font("Tahoma", Font.BOLD, 12));
		panelDuracion.add(lblSoundBar);
		lblSoundBar.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblSoundBar.setForeground(col.getColor(style.getProperty("CTiempoActual")));
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		panelDuracion.add(horizontalStrut_3);
		
		JSlider sliderDuracion = new JSlider();
		sliderDuracion.setValue(0);
		
		sliderDuracion.setBackground(col.getColor(style.getProperty("FMusicaRep")));
		
		panelDuracion.add(sliderDuracion);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		panelDuracion.add(horizontalStrut_4);
		
		JLabel lblDuraciontotal = new JLabel("DuracionTotal");
		lblDuraciontotal.setForeground(col.getColor(style.getProperty("CTiempoFinal")));
		lblDuraciontotal.setFont(new Font("Tahoma", Font.BOLD, 12));
		panelDuracion.add(lblDuraciontotal);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		panelDuracion.add(horizontalStrut_5);
		
		Component verticalStrut_2 = Box.createVerticalStrut(10);
		panelInferior.add(verticalStrut_2);
		
		JPanel panelControlBotonera = new JPanel();
		panelControlBotonera.setBackground(col.getColor(style.getProperty("FBotonera")));
		panelInferior.add(panelControlBotonera);
		panelControlBotonera.setLayout(new BoxLayout(panelControlBotonera, BoxLayout.X_AXIS));
		
		JPanel panelCancion = new JPanel();
		panelCancion.setBackground(col.getColor(style.getProperty("FCancion")));
		panelControlBotonera.add(panelCancion);
		
		JLabel lbliconoImagen = new JLabel("");
		//lbliconoImagen.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"albumIconoHuevo.png"));
		panelCancion.add(lbliconoImagen);
		
		JLabel lblAutorCancion = new JLabel();
		lblAutorCancion.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblAutorCancion.setForeground(col.getColor(style.getProperty("CTituloCancion")));
		panelCancion.add(lblAutorCancion);
		
		JPanel panelBotonera = new JPanel();
		panelBotonera.setBackground(col.getColor(style.getProperty("FBotonera")));
		panelControlBotonera.add(panelBotonera);
		panelBotonera.setLayout(new BoxLayout(panelBotonera, BoxLayout.X_AXIS));
		
		JButton btnAnterior = new JButton("");
		//btnAnterior.setBorderPainted(false);
		btnAnterior.setBackground(Color.WHITE);
		btnAnterior.setFocusable(false);
		
		panelBotonera.add(btnAnterior);
		btnAnterior.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"backicon.png"));
		
		Component horizontalStrut_7 = Box.createHorizontalStrut(5);
		panelBotonera.add(horizontalStrut_7);
		
		JButton btnPlay = new JButton("");
		
		btnPlay.setBackground(Color.WHITE);
		
		panelBotonera.add(btnPlay);
		btnPlay.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"playicon.png"));
		btnPlay.setFocusable(false);
		Component horizontalStrut_8 = Box.createHorizontalStrut(5);
		panelBotonera.add(horizontalStrut_8);
		
		JButton btnPause = new JButton("");
		btnPause.setBackground(Color.WHITE);
		panelBotonera.add(btnPause);
		btnPause.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"pauseicon.png"));
		btnPause.setFocusable(false);
		Component horizontalStrut_10 = Box.createHorizontalStrut(5);
		panelBotonera.add(horizontalStrut_10);
		
		JButton btnSiguiente = new JButton("");
		btnSiguiente.setBackground(Color.WHITE);
		panelBotonera.add(btnSiguiente);
		btnSiguiente.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"nexticon.png"));
		btnSiguiente.setFocusable(false);
		
		Component horizontalStrut_11 = Box.createHorizontalStrut(15);
		panelBotonera.add(horizontalStrut_11);
		
		JButton btnRepetir = new JButton("");
		btnRepetir.setBackground(Color.WHITE);
		panelBotonera.add(btnRepetir);
		btnRepetir.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"repeaticon.png"));
		btnRepetir.setFocusable(false);
		Component horizontalStrut_12 = Box.createHorizontalStrut(5);
		panelBotonera.add(horizontalStrut_12);
		
		JButton btnAleatorio = new JButton("");
		btnAleatorio.setBackground(Color.WHITE);
		panelBotonera.add(btnAleatorio);
		btnAleatorio.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"randomicon.png"));
		btnAleatorio.setFocusable(false);
		
		
		JPanel panelVolumen = new JPanel();
		panelVolumen.setBackground(col.getColor(style.getProperty("FVolumen")));
		panelControlBotonera.add(panelVolumen);
		panelVolumen.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblVolumen = new JLabel(lang.getProperty("Volumen"));
		lblVolumen.setForeground(col.getColor(style.getProperty("CVolumen")));
		lblVolumen.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblVolumen.setBackground(new Color(178, 34, 34));
		lblVolumen.setHorizontalAlignment(SwingConstants.CENTER);
		panelVolumen.add(lblVolumen);
		
		JSlider slider = new JSlider();
		slider.setBackground(col.getColor(style.getProperty("FVolumen")));
		panelVolumen.add(slider);
		
		JPanel panelSuperior = new JPanel();
		panelSuperior.setBackground(col.getColor(style.getProperty("FLogo")));
		frame.getContentPane().add(panelSuperior, BorderLayout.NORTH);
		panelSuperior.setLayout(new BoxLayout(panelSuperior, BoxLayout.X_AXIS));
		
		JPanel panelLogotipo = new JPanel();
		panelLogotipo.setBackground(col.getColor(style.getProperty("FLogo")));
		panelLogotipo.setPreferredSize(new Dimension(142,75));
		panelSuperior.add(panelLogotipo);
		panelLogotipo.setLayout(new BoxLayout(panelLogotipo, BoxLayout.Y_AXIS));
		
		JLabel lblMarca = new JLabel("");
		lblMarca.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblMarca.setForeground(new Color(153, 50, 204));
		lblMarca.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"logoEsquinaBuena.png"));
		panelLogotipo.add(lblMarca);
		
		JPanel panelBuscar = new JPanel();
		panelSuperior.add(panelBuscar);
		panelBuscar.setBackground(col.getColor(style.getProperty("FBusqueda")));
		panelBuscar.setLayout(new BoxLayout(panelBuscar, BoxLayout.Y_AXIS));
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		panelBuscar.add(verticalStrut_1);
		
		JPanel panelBuscarBTN = new JPanel();
		panelBuscarBTN.setBackground(col.getColor(style.getProperty("FBusqueda")));
		panelBuscar.add(panelBuscarBTN);
		
		JLabel lblBuscar = new JLabel("Coffee Sound");
		panelBuscarBTN.add(lblBuscar);
		lblBuscar.setFont(new Font("Tahoma", Font.BOLD, 42));
		lblBuscar.setForeground(col.getColor(style.getProperty("CBuscar")));
		
		JPanel panelPerfil = new JPanel();
		panelPerfil.setBackground(col.getColor(style.getProperty("FUsuario")));
		panelPerfil.setPreferredSize(new Dimension(150, 42));
		//nueva dimension
		panelSuperior.add(panelPerfil);
		panelPerfil.setLayout(new BoxLayout(panelPerfil, BoxLayout.Y_AXIS));
		
		JLabel lblFotoPerfil = new JLabel("");
		lblFotoPerfil.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblFotoPerfil.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"iconousuario2.png"));
		panelPerfil.add(lblFotoPerfil);
		
		JLabel lblNombreUsuario = new JLabel(Usuario.getCurrentUser().getUsername());
		lblNombreUsuario.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNombreUsuario.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNombreUsuario.setForeground(col.getColor(style.getProperty("CUsuario")));
		panelPerfil.add(lblNombreUsuario);
		
		JPanel panelCentral = new JPanel();
		frame.getContentPane().add(panelCentral, BorderLayout.CENTER);


		panelCentral.setLayout(new GridLayout(0, 1, 0, 0));
		
		JScrollPane scrollpanelcentral = new JScrollPane();
		panelCentral.add(scrollpanelcentral);
		PanelUsers pu = new PanelUsers(panelCentral);
		scrollpanelcentral.setViewportView(pu);
//		JScrollPane scrollPane_1 = new JScrollPane();
//		panelCentral.add(scrollPane_1);
		
		
//		scrollPane_1.setViewportView(listaMusic);
		
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		frame.setJMenuBar(menuBar);
		
		mnArchivo = new JMenu(lang.getProperty("Archivo"));
		mnArchivo.setForeground(Color.BLACK);
		menuBar.add(mnArchivo);
		
		JMenu mnAñadir = new JMenu(lang.getProperty("Anadir"));
		mnArchivo.add(mnAñadir);
		
		smnAñadirMusica = new JMenuItem(lang.getProperty("Canciones"));
		mnAñadir.add(smnAñadirMusica);
		

		JMenuItem smnAñadirAlbum = new JMenuItem(lang.getProperty("Albumes"));
		mnAñadir.add(smnAñadirAlbum);
		

		
		

		
		JSeparator separator_3 = new JSeparator();
		mnArchivo.add(separator_3);
		
		JMenuItem smnSalir = new JMenuItem(lang.getProperty("Salir"));
		smnSalir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.META_MASK));
		
		mnArchivo.add(smnSalir);
		
		mnEditar = new JMenu(lang.getProperty("Editar"));
		mnEditar.setForeground(Color.BLACK);
		menuBar.add(mnEditar);
		
		JMenuItem mntmPreferencias = new JMenuItem(lang.getProperty("Ajustes"));
		
		mnEditar.add(mntmPreferencias);
		
		mnControles = new JMenu(lang.getProperty("Controles"));
		mnControles.setForeground(Color.BLACK);
		menuBar.add(mnControles);
		
		smnReproducir = new JMenuItem(lang.getProperty("Reproducir"));
		smnReproducir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, InputEvent.ALT_MASK));
		mnControles.add(smnReproducir);
		
		JSeparator separator = new JSeparator();
		mnControles.add(separator);
		
		smnSiguiente = new JMenuItem(lang.getProperty("Siguiente"));
		smnSiguiente.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.ALT_MASK));
		mnControles.add(smnSiguiente);
		
		smnAnterior = new JMenuItem(lang.getProperty("Anterior"));
		smnAnterior.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.ALT_MASK));
		mnControles.add(smnAnterior);
		
		JSeparator separator_1 = new JSeparator();
		mnControles.add(separator_1);
		
		smnAleatoria = new JMenuItem(lang.getProperty("Aleatorio"));
		smnAleatoria.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
		mnControles.add(smnAleatoria);
		
		smnRepetir = new JMenuItem(lang.getProperty("Repetir"));
		smnRepetir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.ALT_MASK));
		mnControles.add(smnRepetir);
		
		JSeparator separator_2 = new JSeparator();
		mnControles.add(separator_2);
		
		smnSubirVolumen = new JMenuItem(lang.getProperty("SubirVol"));
		smnSubirVolumen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.ALT_MASK));
		mnControles.add(smnSubirVolumen);
		
		smnBajarVolumen = new JMenuItem(lang.getProperty("BajarVol"));
		smnBajarVolumen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.ALT_MASK));
		mnControles.add(smnBajarVolumen);
		
		mnVer = new JMenu(lang.getProperty("Ver"));
		mnVer.setForeground(Color.BLACK);
		menuBar.add(mnVer);
		
		JMenuItem smnRefrescar = new JMenuItem(lang.getProperty("Recargar"));
		smnRefrescar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		
		mnVer.add(smnRefrescar);
		//listaMusic.setModel(controller.actualizarlista());
		//CONTROLLER ACTIONS
		controller.setActionPlay(btnPlay, listaMusic, lblDuraciontotal,sliderDuracion,lblAutorCancion, lblSoundBar);
		controller.setNextSong(btnSiguiente, listaMusic, lblDuraciontotal,sliderDuracion,lblAutorCancion, lblSoundBar);
		controller.setPreviousSong(btnAnterior, listaMusic, lblDuraciontotal,sliderDuracion,lblAutorCancion, lblSoundBar);
		
		controller.setMouseListenerSlider(sliderDuracion,listaMusic,lblDuraciontotal,lblAutorCancion, lblSoundBar);
		controller.setVolumeListener(slider);

		controller.setActionPause(btnPause, listaMusic);
		controller.setVolumeUp(smnSubirVolumen, slider);
		controller.setVolumeDown(smnBajarVolumen, slider);
		controller.setMenuPlay(smnReproducir, listaMusic, lblDuraciontotal,sliderDuracion,lblAutorCancion, lblSoundBar);
		controller.setMenuRefresh(smnRefrescar, listaMusic);
		controller.setMenuAlbum(smnAñadirAlbum);
		controller.setMenuPreferencias(mntmPreferencias, frame);
		controller.setMenuSalir(smnSalir, frame);
		controller.setActionAleatorio(btnAleatorio);
		controller.setActionRepetir(btnRepetir);
		controller.setMenuCancion(smnAñadirMusica);
		
		controller.setIconImage(lblFotoPerfil);
		controller.setActionListaNuevo(list, panelCentral);
		controller.setActionListaMios(list_1, panelCentral);
		}
	
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	
}

