package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ProgressBarUI;

import Idioma.ControlIdioma;

import javax.swing.BoxLayout;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.awt.Component;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Toolkit;

public class Cargando extends JFrame {

	private JPanel contentPane;
	private JProgressBar barraProgr;


	

	public Cargando() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Cargando.class.getResource("/Imagenes/logoEsquinaBuena.png")));
		setTitle("Coffee Sound - "+ControlIdioma.getControlIdioma().getProperty("Cargando"));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(200, 200, 300, 110);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		barraProgr = new JProgressBar();
		barraProgr.setIndeterminate(true);
		panel.add(barraProgr);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.DARK_GRAY);
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JLabel lblCargando = new JLabel(ControlIdioma.getControlIdioma().getProperty("Cargando")+"...");
		lblCargando.setForeground(Color.WHITE);
		lblCargando.setFont(new Font("Lucida Grande", Font.BOLD, 30));
		panel_1.add(lblCargando);
	
	}

}