package Vista;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import Controlador.AlbumController;
import Controlador.ColorString;
import Idioma.ControlIdioma;
import Temas.ControlTemas;
import java.awt.Color;
import java.awt.FlowLayout;

public class CreadorAlbum extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNombreAlbum;
	private JTextField textFieldDescripAlbum;
	private AlbumController controller = null;
	
	ControlTemas style = ControlTemas.getControlTemas();
	ColorString col = new ColorString();
	
	/**
	 * Create the frame.
	 */
	public CreadorAlbum() {
		controller = new AlbumController();
		setIconImage(Toolkit.getDefaultToolkit().getImage(CreadorAlbum.class.getResource("/Imagenes/logoEsquinaBuena.png")));
		setTitle("Coffee Sound - Creador de Album");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(col.getColor(style.getProperty("FPrincipal")));
		FlowLayout flowLayout = (FlowLayout) panel_5.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_5, BorderLayout.NORTH);
		
		JLabel lblCreadorDeAlbum = new JLabel(ControlIdioma.getControlIdioma().getProperty("CreadorAlbum"));
		lblCreadorDeAlbum.setForeground(col.getColor(style.getProperty("CPrincipal")));
		panel_5.add(lblCreadorDeAlbum);
		lblCreadorDeAlbum.setBackground(Color.BLACK);
		lblCreadorDeAlbum.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel.add(panel_1);
		
		JLabel lblNombreAlbum = new JLabel(ControlIdioma.getControlIdioma().getProperty("NombreAlbum"));
		lblNombreAlbum.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_1.add(lblNombreAlbum);
		lblNombreAlbum.setHorizontalAlignment(SwingConstants.CENTER);
		
		textFieldNombreAlbum = new JTextField();
		panel_1.add(textFieldNombreAlbum);
		textFieldNombreAlbum.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel.add(panel_2);
		
		JLabel lblDescripcion = new JLabel(ControlIdioma.getControlIdioma().getProperty("Descripcion"));
		lblDescripcion.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_2.add(lblDescripcion);
		
		textFieldDescripAlbum = new JTextField();
		panel_2.add(textFieldDescripAlbum);
		textFieldDescripAlbum.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel.add(panel_3);
		
		JLabel lblSelecciona = new JLabel(ControlIdioma.getControlIdioma().getProperty("Seleccionar"));
		lblSelecciona.setForeground(col.getColor(style.getProperty("CSencudario")));
		panel_3.add(lblSelecciona);
		
		JButton btnSeleccionar = new JButton(ControlIdioma.getControlIdioma().getProperty("Seleccionar"));
		panel_3.add(btnSeleccionar);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(col.getColor(style.getProperty("FPrincipal")));
		panel.add(panel_4);
		
		JButton btnCancelar = new JButton("Cancelar");
		panel_4.add(btnCancelar);
		
		JButton btnOk = new JButton("OK");
		panel_4.add(btnOk);
		
		controller.setActionSeleccionar(btnSeleccionar);
		controller.setActionNuevo(btnOk, textFieldNombreAlbum, textFieldDescripAlbum);
		controller.setActionCancelar(this, btnCancelar);
	}

}
