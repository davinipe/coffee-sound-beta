package Vista;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.Toolkit;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;

import Controlador.ColorString;
import Controlador.IniciarSesionController;
import Controlador.ReportesController;
import Temas.ControlTemas;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ScrollPaneConstants;
import java.awt.Component;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.FlowLayout;

public class Reportes extends JFrame {

	private JPanel contentPane;
	ControlTemas style = ControlTemas.getControlTemas();
	ColorString col = new ColorString();

	/**
	 * 
	 * Launch the application.
	 */
	ReportesController controller = new ReportesController();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Reportes frame = new Reportes();
					frame.setVisible(true);
					frame.setIconImage(Toolkit.getDefaultToolkit().getImage(CoffeeSound.class.getResource("/Imagenes/logoEsquinaBuena.png")));
					frame.setTitle("Coffee Sound - Reportes");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Reportes() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Reportes.class.getResource("/Imagenes/logotipo2.png")));
		setTitle("Coffee Sound - Reportes");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel_2.setBackground(col.getColor(style.getProperty("FPrincipal")));
		contentPane.add(panel_2, BorderLayout.NORTH);
		
		
		//scrollPane.setRowHeaderView(textArea);
		
		JLabel lblReporte = new JLabel("Reporte");
		lblReporte.setForeground(col.getColor(style.getProperty("CPrinciapl")));
		panel_2.add(lblReporte);
		lblReporte.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		JPanel panel = new JPanel();
		panel.setBackground(col.getColor(style.getProperty("FPrincipal")));
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBackground(col.getColor(style.getProperty("FPrincipal")));
		panel.add(btnEnviar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(col.getColor(style.getProperty("FPrincipal")));	
		panel.add(btnCancelar);
		
		ButtonGroup group = new ButtonGroup();
		controller.cerrarVentana(btnCancelar, this);
		
		JPanel pnaelCenter = new JPanel();
		pnaelCenter.setBackground(col.getColor(style.getProperty("FSecundario")));
		contentPane.add(pnaelCenter, BorderLayout.CENTER);
		pnaelCenter.setLayout(new BoxLayout(pnaelCenter, BoxLayout.X_AXIS));
		
		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		
		JScrollPane scrollPane = new JScrollPane(textArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pnaelCenter.add(scrollPane);
		
		JLabel lblDescripcin = new JLabel("Descripción");
		lblDescripcin.setBackground(Color.WHITE);
		lblDescripcin.setHorizontalAlignment(SwingConstants.CENTER);
		scrollPane.setColumnHeaderView(lblDescripcin);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(col.getColor(style.getProperty("FSecundario")));
		pnaelCenter.add(panel_1);
		
		JLabel lblNewLabel = new JLabel("Motivo:");
		lblNewLabel.setForeground(col.getColor(style.getProperty("CSecundario")));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JRadioButton rdbtnCopyright = new JRadioButton("Copyright");
		rdbtnCopyright.setForeground(col.getColor(style.getProperty("CSecundario")));
		

		
		JRadioButton rdbtnArchivoDaado = new JRadioButton("Archivo dañado");
		rdbtnArchivoDaado.setForeground(col.getColor(style.getProperty("CSecundario")));
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Infracción de derechos");
		rdbtnNewRadioButton.setForeground(col.getColor(style.getProperty("CSecundario")));
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Contenido engañoso");
		rdbtnNewRadioButton_1.setForeground(col.getColor(style.getProperty("CSecundario")));

		JRadioButton rdbtnOtroMotivo = new JRadioButton("Otro motivo");
		rdbtnOtroMotivo.setForeground(col.getColor(style.getProperty("CSecundario")));
		
		group.add(rdbtnCopyright);
		group.add(rdbtnArchivoDaado);
		group.add(rdbtnNewRadioButton);
		group.add(rdbtnNewRadioButton_1);
		group.add(rdbtnOtroMotivo);
		
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(5)
							.addComponent(lblNewLabel))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(rdbtnCopyright))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(rdbtnArchivoDaado))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(rdbtnNewRadioButton))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(rdbtnNewRadioButton_1))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(rdbtnOtroMotivo)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(5)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(rdbtnCopyright)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnArchivoDaado)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnNewRadioButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnNewRadioButton_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnOtroMotivo)
					.addContainerGap(36, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		
		
		controller.compruebaReporte(btnEnviar, textArea, rdbtnCopyright, rdbtnArchivoDaado, rdbtnNewRadioButton, rdbtnNewRadioButton_1, rdbtnOtroMotivo);
		
	
	}
}
