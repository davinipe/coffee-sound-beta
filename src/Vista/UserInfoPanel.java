package Vista;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.net.ftp.FTPClient;

import Controlador.ColorString;
import Idioma.ControlIdioma;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import Objetos.FTPConnection;
import Temas.ControlTemas;

public class UserInfoPanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public UserInfoPanel(Usuario user) {
		ColorString col = new ColorString();
		setBackground(col.getColor(ControlTemas.getControlTemas().getProperty("FAutor")));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel image = new JLabel();
		image.setAlignmentX(Component.CENTER_ALIGNMENT);
		setIconImage(image, user);
		this.add(image);
		JLabel username = new JLabel(user.getUsername().toUpperCase());
		username.setAlignmentX(Component.CENTER_ALIGNMENT);
		username.setForeground(Color.WHITE);
		JLabel mail = new JLabel();
		mail.setText(user.getCorreo());
		mail.setAlignmentX(Component.CENTER_ALIGNMENT);
		mail.setForeground(Color.WHITE);
		JLabel songs = new JLabel("Canciones: "+UsuarioMetodos.getNumeroCanciones(user));
		songs.setAlignmentX(Component.CENTER_ALIGNMENT);
		songs.setForeground(Color.WHITE);
		this.add(image);
		this.add(username);
		this.add(mail);
		this.add(songs);
	}
	
	public void setIconImage(JLabel label, Usuario user){
		FTPClient client = FTPConnection.getClient();
		if(user.getFoto().length()>0){
		try {
			File file = new File(user.getFoto().substring(user.getFoto().lastIndexOf('/')+1,user.getFoto().length()));
			if(!file.exists()){
				file.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(file);
			if(client.retrieveFile(user.getFoto(), fos)){
				fos.close();
				Image i = new ImageIcon(file.getName()).getImage().getScaledInstance(55, 70, Image.SCALE_SMOOTH);
				ImageIcon ii = new ImageIcon(i);
				 
				BufferedImage master = new BufferedImage(ii.getIconWidth(), ii.getIconHeight(), BufferedImage.TYPE_4BYTE_ABGR);
				// or use any other fitting type
				
				master.getGraphics().drawImage(i, 0, 0, null);
			 
			 

			    int diameter = 55;
			    BufferedImage mask = new BufferedImage(master.getWidth(), master.getHeight(), BufferedImage.TYPE_INT_ARGB);

			    Graphics2D g2d = mask.createGraphics();
			    applyQualityRenderingHints(g2d);
			    g2d.fillOval(0, 0, diameter - 1, diameter - 1);
			    g2d.dispose();

			    BufferedImage masked = new BufferedImage(diameter, diameter, BufferedImage.TYPE_INT_ARGB);
			    g2d = masked.createGraphics();
			    applyQualityRenderingHints(g2d);
			    int x = (diameter - master.getWidth(null)) / 2;
			    int y = (diameter - master.getHeight(null)) / 2;
			    g2d.drawImage(master, x, y, null);
			    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.DST_IN));
			    g2d.drawImage(mask, 0, 0, null);
			    g2d.dispose();

				label.setIcon(new ImageIcon(masked));
			}else{
				fos.close();
				file.delete();
			}
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
	
	public static void applyQualityRenderingHints(Graphics2D g2d) {

	    g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
	    g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

	}
}
