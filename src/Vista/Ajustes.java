package Vista;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import Confg.CrearPreferencias;
import Confg.Preferencias;
import Controlador.CoffeeSoundController;
import Controlador.ColorString;
import Idioma.ControlIdioma;
import Modelo.ActiveDirectory;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import Temas.ControlTemas;

public class Ajustes extends JFrame {

	private JPanel contentPane;
	Preferencias conf = new Preferencias();
	ControlIdioma lang = ControlIdioma.getControlIdioma();
	ControlTemas style = ControlTemas.getControlTemas();
	ColorString col = new ColorString();
	private JFrame f= this;


	/**
	 * Create the frame.
	 */
	public Ajustes(JFrame principal, CoffeeSoundController csc) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ajustes.class.getResource("/Imagenes/logoEsquinaBuena.png")));
		setTitle("Coffee Sound - Ajustes");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelSuperior = new JPanel();
		panelSuperior.setBackground(col.getColor(style.getProperty("FPrincipal")));
		contentPane.add(panelSuperior, BorderLayout.NORTH);
		
		JLabel lblAjustes = new JLabel(lang.getProperty("Ajustes"));
		lblAjustes.setForeground(col.getColor(style.getProperty("CPrincipal")));
		panelSuperior.add(lblAjustes);
		lblAjustes.setVerticalAlignment(SwingConstants.TOP);
		lblAjustes.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAjustes.setHorizontalAlignment(SwingConstants.LEFT);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(col.getColor(style.getProperty("FSecundario")));
		contentPane.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel_2.add(panel_3);
		
		JLabel lblIdioma = new JLabel(lang.getProperty("Idioma"));
		lblIdioma.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_3.add(lblIdioma);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setForeground(col.getColor(style.getProperty("CSecundarioCMBX")));
		panel_3.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {lang.getProperty("Espanol"), lang.getProperty("Ingles")}));
		comboBox.setSelectedIndex(conf.getNidioma());
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel_2.add(panel_4);
		
		JCheckBox chckbxPremium = new JCheckBox("Premium");
		chckbxPremium.setBackground(col.getColor(style.getProperty("FSecundario")));
		chckbxPremium.setForeground(Color.BLACK);
		panel_4.add(chckbxPremium);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(col.getColor(style.getProperty("FSecundario")));
		panel_2.add(panel_5);
		
		JLabel lblTema = new JLabel(lang.getProperty("Tema"));
		lblTema.setForeground(col.getColor(style.getProperty("CSecundario")));
		panel_5.add(lblTema);
		
		JComboBox comboBoxTema = new JComboBox();
		comboBoxTema.setModel(new DefaultComboBoxModel(new String[] {"Minimalista ~ Jorge López Gil", "Neon ~ Anónimo"}));
		comboBoxTema.setSelectedIndex(conf.getnTema());
		comboBoxTema.setForeground(col.getColor(style.getProperty("CSecundarioCMBX")));
		panel_5.add(comboBoxTema);
		
		JPanel panelInferior = new JPanel();
		panelInferior.setBackground(col.getColor(style.getProperty("FPrincipal")));
		contentPane.add(panelInferior, BorderLayout.SOUTH);
		
		JButton btnAplicar = new JButton(lang.getProperty("Aplicar"));
		btnAplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				CrearPreferencias.leerPreferncias();
				
				//Ventana cargando
				Cargando carga = new Cargando();
				carga.setVisible(true);
				Thread thread = new Thread(new Runnable() {
					
					@Override
					public void run() {
						//Idioma
						switch(comboBox.getSelectedIndex()){
						case 0:
							conf.setIdioma("ES");
							CrearPreferencias.p.setIdioma("ES");
							conf.setNidioma(0);
							break;
						case 1:
							conf.setIdioma("ENG");
							CrearPreferencias.p.setIdioma("ENG");
							conf.setNidioma(1);
							break;
						}
						lang.setIdioma(conf.getIdioma());
					
					//Tema
						switch(comboBoxTema.getSelectedIndex()){
						case 0:
							conf.setTema("minimal");
							CrearPreferencias.p.setTema("minimal");
							conf.setnTema(0);
							break;
						case 1:
							conf.setTema("neon");
							CrearPreferencias.p.setTema("neon");
							conf.setnTema(1);
							break;
						}
						style.setTema(conf.getTema());
						CrearPreferencias.CrearPreferencias();
					
					//sdispose();
					f.setVisible(false);
					principal.setVisible(false);
					JFrame coffe = new CoffeeSound().frame;
					csc.player.stopMusic();
					principal.dispose();
					carga.dispose();
					coffe.setVisible(true);
					
					System.out.println(lang.getProperty("Aplicar"));							
						
					}
				});
				thread.start();

			}
		});
		panelInferior.add(btnAplicar);
		
		
		JButton btnCancelar = new JButton(lang.getProperty("Cancelar"));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		panelInferior.add(btnCancelar);
		
		JButton btnCerrarSesin = new JButton(lang.getProperty("CerrarSesion"));
		panelInferior.add(btnCerrarSesin);
	}

}
