package Vista;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Controlador.PanelUsersController;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import java.awt.Component;

public class PanelUsers extends JPanel {
	PanelUsersController controller = null;
	/**
	 * Create the panel.
	 */
	public PanelUsers(JPanel panelcentral) {
		controller = new PanelUsersController();
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setHgap(20);
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		
		
		
		ArrayList<Usuario> usuarios = UsuarioMetodos.getUsuarios();
		for(Usuario user: usuarios){
			JPanel userpanel = new JPanel();
			userpanel.setMinimumSize(new Dimension(90,90));
			userpanel.setMaximumSize(new Dimension(90,90));
			userpanel.setPreferredSize(new Dimension(90,90));
			userpanel.setBackground(Color.DARK_GRAY);
			userpanel.setLayout(new BoxLayout(userpanel, BoxLayout.Y_AXIS));
			JLabel profile = new JLabel("");
			profile.setAlignmentX(Component.CENTER_ALIGNMENT);
			profile.setForeground(Color.WHITE);
			userpanel.add(profile);
			JButton userbutton = new JButton(user.getUsername());
			userbutton.setAlignmentY(Component.BOTTOM_ALIGNMENT);
			userbutton.setAlignmentX(Component.CENTER_ALIGNMENT);
			userbutton.setForeground(Color.WHITE);
			userbutton.setBackground(Color.BLACK);
			userbutton.setPreferredSize(new Dimension(90,20));
			userbutton.setMaximumSize(new Dimension(90,20));
			userbutton.setMinimumSize(new Dimension(90,20));
			userpanel.add(userbutton);
			
			this.add(userpanel);
			controller.setPanelAction(userbutton, this, panelcentral);
			controller.setIconImage(profile, user);
		}
	}

}
