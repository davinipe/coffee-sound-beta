package Vista;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Confg.CrearPreferencias;
import Confg.Preferencias;
import Controlador.IniciarSesionController;
import Idioma.ControlIdioma;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.Box;
import java.awt.FlowLayout;
import java.awt.Color;

public class IniciarSesion extends JFrame {

	private JPanel contentPane;
	private JTextField txtCorreo;

	//Idiomas
	Preferencias conf = new Preferencias();
	ControlIdioma lang = ControlIdioma.getControlIdioma();
	private JPasswordField txtPassword;


	IniciarSesionController controller = new IniciarSesionController();

	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearPreferencias.leerPreferncias();
					IniciarSesion frame = new IniciarSesion();
					frame.setVisible(true);
					frame.setIconImage(Toolkit.getDefaultToolkit().getImage(CoffeeSound.class.getResource("/Imagenes/logoEsquinaBuena.png")));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IniciarSesion() {
		try{
			setIconImage(Toolkit.getDefaultToolkit().getImage(IniciarSesion.class.getResource("/Imagenes/logoEsquinaBuena.png")));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en la mierda del icono");
		}
		setTitle("Coffee Sound - " + lang.getProperty("IniciarSesion"));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 397);
		contentPane = new JPanel();
		contentPane.setForeground(Color.WHITE);
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JPanel panelSuperior = new JPanel();
		panelSuperior.setBackground(Color.DARK_GRAY);

		contentPane.add(panelSuperior);

		contentPane.add(panelSuperior, BorderLayout.NORTH);
		panelSuperior.setLayout(new BoxLayout(panelSuperior, BoxLayout.Y_AXIS));

		
		JLabel lblSoundCoffee = new JLabel(lang.getProperty("SoundCoffee"));
		lblSoundCoffee.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblSoundCoffee.setForeground(Color.WHITE);
		lblSoundCoffee.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		panelSuperior.add(lblSoundCoffee);
		
		JLabel lblRecuContra = new JLabel("");
		lblRecuContra.setAlignmentX(Component.CENTER_ALIGNMENT);
		Image i = new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"logoInicio.png").getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
		lblRecuContra.setIcon(new ImageIcon(i));
		panelSuperior.add(lblRecuContra);
		
		Component verticalGlue_1 = Box.createVerticalGlue();
		contentPane.add(verticalGlue_1);
		
		JPanel panelCentro = new JPanel();
		panelCentro.setBackground(Color.DARK_GRAY);
		contentPane.add(panelCentro);
		panelCentro.setLayout(new BoxLayout(panelCentro, BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panelCentro.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JLabel lblNewLabel = new JLabel(lang.getProperty("Usuario"));		
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		lblNewLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel.add(lblNewLabel);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		panel.add(verticalStrut_1);
		
		JLabel lblNewLabel_1 = new JLabel(lang.getProperty("Contrasena"));
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		lblNewLabel_1.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel.add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.DARK_GRAY);
		panelCentro.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		txtCorreo = new JTextField();
		txtCorreo.setAlignmentX(Component.LEFT_ALIGNMENT);
		txtCorreo.setPreferredSize(new Dimension(150,20));
		txtCorreo.setMaximumSize(new Dimension(150,20));
		panel_1.add(txtCorreo);
		txtCorreo.setColumns(10);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut);
		
		txtPassword = new JPasswordField();
		txtPassword.setAlignmentX(Component.LEFT_ALIGNMENT);
		txtPassword.setPreferredSize(new Dimension(150,20));
		txtPassword.setMaximumSize(new Dimension(150,20));
		panel_1.add(txtPassword);
		txtPassword.setColumns(10);
		
		Component verticalGlue = Box.createVerticalGlue();
		contentPane.add(verticalGlue);
		
		
		
		JPanel panelInferior = new JPanel();
		panelInferior.setBackground(Color.DARK_GRAY);
		contentPane.add(panelInferior);
		panelInferior.setLayout(new BoxLayout(panelInferior, BoxLayout.X_AXIS));
		
		JPanel panelII = new JPanel();
		panelII.setBackground(Color.DARK_GRAY);
		panelInferior.add(panelII);
		
		JButton btnIS = new JButton(lang.getProperty("IniciarSesion"));

		btnIS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		btnIS.setForeground(Color.BLACK);
		btnIS.setBackground(Color.WHITE);

		panelII.add(btnIS);
		
		JButton btnRC = new JButton(lang.getProperty("RecuperarContrasena"));
		btnRC.setForeground(Color.BLACK);
		btnRC.setBackground(Color.WHITE);
		panelII.add(btnRC);
		
		controller.compruebaUsuario(txtCorreo, txtPassword, btnIS, this);
		controller.recuperarContra(btnRC, txtCorreo, lblRecuContra);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.DARK_GRAY);
		panelInferior.add(panel_2);
		
		JButton btnRegistrarse = new JButton(lang.getProperty("Registrarse"));
		panel_2.add(btnRegistrarse);
		btnRegistrarse.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnRegistrarse.setForeground(Color.BLACK);
		btnRegistrarse.setBackground(Color.WHITE);
		btnRegistrarse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		controller.registroBoton(btnRegistrarse, lang);
	}

}
