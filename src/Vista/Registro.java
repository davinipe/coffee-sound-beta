package Vista;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Confg.Preferencias;
import Controlador.RegistroController;
import Idioma.ControlIdioma;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Registro extends JFrame {

	private JPanel contentPane;
	private JTextField txtCorreo;
	private JTextField txtUser;
	private JTextField txtApedillos;
	private JTextField txtNombre;
	private JPasswordField passwordField;
	private RegistroController rc = null;

	//Idiomas
	Preferencias conf = new Preferencias();
	ControlIdioma lang = ControlIdioma.getControlIdioma();
	
	
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Registro() {
		rc = new RegistroController();
		setIconImage(Toolkit.getDefaultToolkit().getImage(Registro.class.getResource("/Imagenes/logoEsquinaBuena.png")));
		setTitle("Coffee Sound - Registro");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelRegistro = new JPanel();
		panelRegistro.setBackground(Color.DARK_GRAY);
		contentPane.add(panelRegistro, BorderLayout.NORTH);
		
		JLabel lblRegistro = new JLabel(lang.getProperty("Registro"));
		lblRegistro.setForeground(Color.WHITE);
		lblRegistro.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		panelRegistro.add(lblRegistro);
		
		JPanel panelCenter = new JPanel();
		contentPane.add(panelCenter, BorderLayout.CENTER);
		panelCenter.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panelCorreo = new JPanel();
		panelCorreo.setBackground(Color.DARK_GRAY);
		panelCenter.add(panelCorreo);
		
		JLabel lblCorreo = new JLabel(lang.getProperty("Correo"));
		lblCorreo.setForeground(Color.WHITE);
		panelCorreo.add(lblCorreo);
		
		txtCorreo = new JTextField();
		panelCorreo.add(txtCorreo);
		txtCorreo.setColumns(10);
		
		JPanel panelPass = new JPanel();
		panelPass.setBackground(Color.DARK_GRAY);
		panelCenter.add(panelPass);
		
		JLabel lblContrasea = new JLabel(lang.getProperty("Contrasena"));
		lblContrasea.setForeground(Color.WHITE);
		panelPass.add(lblContrasea);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(10);
		passwordField.setEchoChar('*');
		passwordField.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		panelPass.add(passwordField);
		
		JPanel panelNombre = new JPanel();
		panelNombre.setBackground(Color.DARK_GRAY);
		panelCenter.add(panelNombre);
		
		JLabel lblNombre = new JLabel(lang.getProperty("Nombre"));
		lblNombre.setForeground(Color.WHITE);
		panelNombre.add(lblNombre);
		
		txtNombre = new JTextField();
		panelNombre.add(txtNombre);
		txtNombre.setColumns(10);
		
		JPanel panelApedillos = new JPanel();
		panelApedillos.setBackground(Color.DARK_GRAY);
		panelCenter.add(panelApedillos);
		
		JLabel lblApedillos = new JLabel(lang.getProperty("Apedillos"));
		lblApedillos.setForeground(Color.WHITE);
		panelApedillos.add(lblApedillos);
		
		txtApedillos = new JTextField();
		panelApedillos.add(txtApedillos);
		txtApedillos.setColumns(10);
		
		JPanel panelUsuario = new JPanel();
		panelUsuario.setBackground(Color.DARK_GRAY);
		panelCenter.add(panelUsuario);
		
		JLabel lblUsuario = new JLabel(lang.getProperty("Usuario"));
		lblUsuario.setForeground(Color.WHITE);
		panelUsuario.add(lblUsuario);
		
		txtUser = new JTextField();
		panelUsuario.add(txtUser);
		txtUser.setColumns(10);
		
		JPanel panelImage = new JPanel();
		panelImage.setBackground(Color.DARK_GRAY);
		panelCenter.add(panelImage);
		
		JLabel lblFotoDePerfil = new JLabel("Foto de Perfil");
		lblFotoDePerfil.setForeground(Color.WHITE);
		panelImage.add(lblFotoDePerfil);
		
		JButton btnSeleccionar = new JButton(lang.getProperty("Seleccionar"));
		btnSeleccionar.setForeground(Color.BLACK);
		btnSeleccionar.setBackground(Color.WHITE);
		panelImage.add(btnSeleccionar);
		
		JPanel panelInferior = new JPanel();
		panelInferior.setBackground(Color.DARK_GRAY);
		contentPane.add(panelInferior, BorderLayout.SOUTH);
		
		JButton btnCancelar = new JButton(lang.getProperty("Cancelar"));
		btnCancelar.setForeground(Color.BLACK);
		btnCancelar.setBackground(Color.WHITE);
		
		panelInferior.add(btnCancelar);
		
		JButton btnRegistrar = new JButton(lang.getProperty("Registrarse"));
		btnRegistrar.setForeground(Color.BLACK);
		btnRegistrar.setBackground(Color.WHITE);
		panelInferior.add(btnRegistrar);
		
		
		//Controlador
		rc.setActionSeleccionar(btnSeleccionar,txtUser);
		rc.setActionCancelar(btnCancelar, this);
		rc.setActionRegistrar(this, btnRegistrar, txtUser, passwordField, txtCorreo, txtNombre, txtApedillos);
		
	}

}
