package Controlador;

import java.awt.Color;

public class ColorString {
	
	public Color getColor(String code){
		//Separamos en array
		try{
			String[] codes = code.split(",");
					
			int r = Integer.parseInt(codes[0]);
			int g = Integer.parseInt(codes[1]);
			int b = Integer.parseInt(codes[2]);
					
			Color paint = new Color(r, g, b);
			return paint;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en el color, se intento partir --> " + code);
			Color blanco = new Color(255,255,255);
			return blanco;
		}
	}
}
