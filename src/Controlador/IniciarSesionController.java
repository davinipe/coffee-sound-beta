package Controlador;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Confg.CrearPreferencias;
import Idioma.ControlIdioma;
import Modelo.ActiveDirectory;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import Vista.Cargando;
import Vista.CoffeeSound;
import Vista.Registro;

public class IniciarSesionController {
	
	public IniciarSesionController(){

	}

	public void compruebaUsuario(JTextField usu, JPasswordField contra, JButton inicio, JFrame frame){
		inicio.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				Cargando carga = new Cargando();
				carga.setVisible(true);
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						if (usu.getText().isEmpty() || contra.getPassword().toString().isEmpty()) {
							JOptionPane.showMessageDialog(null, "Correo o contrasena vacios");
						} else {
							
						
							if (usu.getText().equals("Administrador")) {
								ActiveDirectory.connect(usu.getText(), String.valueOf(contra.getPassword()));
							}
							Usuario user = new Usuario();
							user.setUsername(usu.getText());
							// HAS A SHA1, JDBC NO ES CAPAZ DE HACERLO ASÍ QUE SE HACE A
							// MANO JE
							String hashStr = null;
							String password = String.valueOf(contra.getPassword());
							try {
								MessageDigest md = MessageDigest.getInstance("SHA-1");
								md.update(password.getBytes());
								BigInteger hash = new BigInteger(1, md.digest());
								hashStr = hash.toString(16);
							} catch (NoSuchAlgorithmException ex) {
								ex.printStackTrace();
							}
							user.setPassword(hashStr);
							if (UsuarioMetodos.checkUser(user)) {
								UsuarioMetodos.setCurrentUser(user);
								CoffeeSound cs = new CoffeeSound();
								cs.frame.setVisible(true);
								carga.dispose();
								frame.dispose();
							}else{
								carga.dispose();
								JOptionPane.showMessageDialog(null,
										"Usuario o contraseña no válidos");
								
							}
						}
					}
				});
				thread.start();				
			}

			});
			
		}

	public void recuperarContra(JButton recuperar, JTextField usu, JLabel mensaje) {
		recuperar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (usu.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Introduce un correo para crear nueva contrasena");
				} else {
					JOptionPane.showMessageDialog(null,
							"Hemos enviado un mensaje al correo introducido. Siga los pasos para recuperar su contrasena");
				}
			}

		});
	}

	public void registroBoton(JButton registrarse, ControlIdioma lang) {
		registrarse.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Registro frame = new Registro();
				frame.setVisible(true);
				frame.setIconImage(
						Toolkit.getDefaultToolkit().getImage(CoffeeSound.class.getResource("/Imagenes/logotipo2.png")));
				frame.setTitle("Coffee Sound - " + lang.getProperty("Registro"));

			}

		});
	}

	public void carga(JTextField usu, JPasswordField contra, JButton inicio, JFrame frame) {
		inicio.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (!usu.getText().isEmpty() || !contra.getPassword().toString().isEmpty()) {
					Thread carga = new Thread();

				}

			}

		});
	}
}


