package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import Modelo.Album;
import Modelo.AlbumMetodos;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import Objetos.FTPConnection;

public class RegistroController {
	FTPClient client = null;
	File file = null;
	
	public RegistroController() {
		this.client=FTPConnection.getClient();
		
	}
	
	public void setActionCancelar(JButton button, JFrame frame){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				frame.dispose();
				
			}
		});
	}
	
	public void setActionRegistrar(JFrame maiki, JButton button, JTextField username, JPasswordField password, JTextField correo, JTextField nombre, JTextField apellidos){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!username.getText().isEmpty() && !password.getPassword().toString().isEmpty() && !correo.getText().isEmpty() && !nombre.getText().isEmpty() && !apellidos.getText().isEmpty()){
					Usuario registeruser = new Usuario();
					registeruser.setNombre(nombre.getText());
					registeruser.setApellidos(apellidos.getText());
					registeruser.setCorreo(correo.getText());
					registeruser.setPassword(String.valueOf(password.getPassword()));
					registeruser.setUsername(username.getText());
					if(file!=null){
						registeruser.setFoto("/"+registeruser.getUsername()+"/profile"+file.getName().substring(file.getName().lastIndexOf('.'),file.getName().length()));
						
					}else{
						registeruser.setFoto("/default.png");
					}
					UsuarioMetodos.insertUser(registeruser);
					registeruser = UsuarioMetodos.getUsuario(registeruser.getUsername());
					Album defaultAlbum = new Album();
					defaultAlbum.setNombre("Default");
					defaultAlbum.setIduser(registeruser.getId());
					defaultAlbum.setImagen("");
					defaultAlbum.setDescripcion("Album por defecto");
					AlbumMetodos.insertAlbum(defaultAlbum, defaultAlbum.getIduser());
					
					JOptionPane.showMessageDialog(null, "Se ha creado la cuenta");
					maiki.dispose();
					
					try {
						client.makeDirectory("/"+username.getText());
						client.makeDirectory("/"+username.getText()+"/albumes");
						client.makeDirectory("/"+username.getText()+"/albumes/default");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(file!=null){
						try {
							client.setFileType(FTP.BINARY_FILE_TYPE);
							FileInputStream fis = new FileInputStream(file);
							
							client.changeWorkingDirectory("/"+username.getText());
							client.storeFile("profile"+file.getName().substring(file.getName().lastIndexOf('.'),file.getName().length()), fis );
							fis.close();
							client.changeWorkingDirectory("/");
						} catch (Exception ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
					}
					
				}
				
			}
		});
	}
	
	public void setActionSeleccionar(JButton button, JTextField user){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				final JFileChooser fc = new JFileChooser();
				FileFilter filter = new FileNameExtensionFilter("Imagenes","png","jpg","jpeg");
				fc.setFileFilter(filter);
				//In response to a button click:
				int returnVal = fc.showOpenDialog(button);
				if(fc.isFileSelectionEnabled() && fc.getSelectedFile()!=null){
						file=fc.getSelectedFile();
						button.setText(file.getName());
						
						
				}
				
				
				
			}
		});
	}

}
