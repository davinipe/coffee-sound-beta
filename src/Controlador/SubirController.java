package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import Modelo.Album;
import Modelo.Cancion;
import Modelo.CancionMetodos;
import Modelo.Categoria;
import Modelo.Usuario;
import Objetos.FTPConnection;

public class SubirController {
	File file = null;
	FTPClient client = null;
	public SubirController() {
		
		this.client=FTPConnection.getClient();
		
	}
	
	public void setActionSeleccionar(JButton button){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//Create a file chooser
				final JFileChooser fc = new JFileChooser();
				FileFilter filter = new FileNameExtensionFilter("Archivo MP3", "mp3");
				fc.setFileFilter(filter);
				//In response to a button click:
				int returnVal = fc.showOpenDialog(button);
				if(fc.isFileSelectionEnabled()&&fc.getSelectedFile()!=null){
					if(fc.getSelectedFile().getName().indexOf(".mp3")>0){
						file=fc.getSelectedFile();
						button.setText(file.getName());
					}
					
				}
				
			}
		});
	}
	
	public void setActionSubir(JButton button, JTextField cancion, JComboBox comboAlbum, JComboBox comboCat, ArrayList<Album> albumes, ArrayList<Categoria> categorias ){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(file!=null && !cancion.getText().isEmpty()){
					try {
						//DB
						Cancion song = new Cancion();
						song.setIdUser(Usuario.getCurrentUser().getId());
						song.setNombre(cancion.getText());
						song.setAlbum(albumes.get(comboAlbum.getSelectedIndex()).getIdalbum());
						song.setCategoria(categorias.get(comboCat.getSelectedIndex()).getId());
						song.setDuracion(CancionMetodos.getDuracion(file));
						song.setUrl("/"+Usuario.getCurrentUser().getUsername()+"/albumes/"+albumes.get(comboAlbum.getSelectedIndex()).getNombre()+"/"+cancion.getText()+".mp3");
						CancionMetodos.insertCancion(song);
						//FTP
						
						client.changeWorkingDirectory("/"+Usuario.getCurrentUser().getUsername()+"/albumes/"+albumes.get(comboAlbum.getSelectedIndex()).getNombre());
						client.setFileType(FTP.BINARY_FILE_TYPE);
						FileInputStream fis = new FileInputStream(file);
						client.storeFile(cancion.getText()+".mp3", fis);
						fis.close();
						client.changeWorkingDirectory("/");
						
						JOptionPane.showMessageDialog(null,"La canción se ha subido correctamente al servidor");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,"Ha habido un error durante la subida");
					}
				}
				
			}
		});
	}

	public void setActionCancelar(JButton button, JFrame frame){
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				frame.dispose();
				
			}
		});
	}
}
