package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.net.ftp.FTPClient;

import Modelo.Album;
import Modelo.AlbumMetodos;
import Modelo.Usuario;
import Objetos.FTPConnection;

public class AlbumController {
	File file= null;
	FTPClient client = null;
	
	public AlbumController() {
		client = FTPConnection.getClient();
	}
	
	public void setActionSeleccionar(JButton button){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final JFileChooser fc = new JFileChooser();
				FileFilter filter = new FileNameExtensionFilter("Imagenes","png","jpg","jpeg");
				fc.setFileFilter(filter);
				//In response to a button click:
				int returnVal = fc.showOpenDialog(button);
				if(fc.isFileSelectionEnabled()){
						file=fc.getSelectedFile();
						button.setText(file.getName());
				}
			}
		});
	}
	
	public void setActionNuevo(JButton button, JTextField nombre, JTextField descripcion){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!nombre.getText().isEmpty() && !descripcion.getText().isEmpty()){
					Album a = new Album();
					a.setNombre(nombre.getText());
					a.setDescripcion(descripcion.getText());
					a.setIduser(Usuario.getCurrentUser().getId());
					
					
					try {
						client.makeDirectory("/"+Usuario.getCurrentUser().getUsername()+"/albumes/"+a.getNombre());
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					
					try {
						if(file!=null){
							client.changeWorkingDirectory("/"+Usuario.getCurrentUser().getUsername()+"/"+a.getNombre());
							FileInputStream fis = new FileInputStream(file);
							client.storeFile("album_image"+file.getName().substring(file.getName().lastIndexOf('.'),file.getName().length()),fis);
							a.setImagen("/"+Usuario.getCurrentUser().getUsername()+"/"+a.getNombre()+"/"+"album_image"+file.getName().substring(file.getName().lastIndexOf('.'),file.getName().length()));;
						}
						
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, "No todos los campos están llenos");
					}
					AlbumMetodos.insertAlbum(a, a.getIduser());
					
				}
				
			}
		});
		
	}
	public void setActionCancelar(JFrame ventana, JButton button){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ventana.dispose();
				
				
			}
		});
	}
	
		
	

}
