package Controlador;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import Confg.Preferencias;
import Modelo.AlbumMetodos;
import Modelo.Cancion;
import Modelo.CancionMetodos;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import Objetos.FTPConnection;
import Objetos.Playing;
import Objetos.VolumeControl;
import Vista.Ajustes;
import Vista.AlbumPanel;
import Vista.Cargando;
import Vista.CoffeeSound;
import Vista.CreadorAlbum;
import Vista.MusicPanel;
import Vista.PanelUsers;
import Vista.Subir;

public class CoffeeSoundController {
	public Playing player = null;
	int max = 0;
	FTPClient client = null;
	FTPFile[] files = null;

	

	boolean repeticion = false;
	Preferencias conf = new Preferencias();
	boolean aleatorio=false;
	CoffeeSoundController csc = this;
	
	

	
	public CoffeeSoundController(){
		player=new Playing();
		this.client=FTPConnection.getClient();
		try {
			this.files=client.listFiles();
		} catch (IOException e) {
			
		}
	}

	public void setVolumeListener(JSlider slider){
		slider.getModel().setMaximum(100);;
		slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				VolumeControl.getVC().setVolume((VolumeControl.getVC().volumeControl.getMaximum()*slider.getValue())/slider.getMaximum());
				System.out.println(VolumeControl.volume);
			}
		});
		slider.addMouseWheelListener(new MouseWheelListener() {
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				slider.setValue(slider.getValue()-(e.getWheelRotation()*2));
				
			}
		});
	}
	public void setMouseListenerSlider(JSlider slider, JList list, JLabel label, JLabel autor, JLabel currentTime){
		slider.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
					createPlayer(list,label,slider,currentTime);
					setAutor(autor);
					player.playMusic(slider.getValue());
					//System.out.println("slider value : "+slider.getValue()+" slider max: "+slider.getMaximum());
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(player.isLoaded()){
					player.stopMusic();
				}
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void setAutor(JLabel autor){
		autor.setText(UsuarioMetodos.getUsuario(Cancion.getListaRep().get(CoffeeSound.listaMusic.getSelectedIndex()).getIdUser()).getUsername()+"-"+Cancion.getListaRep().get(CoffeeSound.listaMusic.getSelectedIndex()).getNombre());
	}
	
	public void setMenuPlay(JMenuItem menuItem, JList list, JLabel label, JSlider slider, JLabel autor, JLabel currentTime){
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(!list.isSelectionEmpty()){
					if(player.isLoaded() && player.isPaused()){
						player.playMusic();
					}else{
						createPlayer(list,label,slider,currentTime);
						setAutor(autor);
						player.playMusic(0);
					}
					
				}else{
					System.out.println("nothing selected");
				}
			}
			
			
		});
	}
	
	public void setActionPlay(JButton button, JList list, JLabel label, JSlider slider, JLabel autor, JLabel currentTime){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!list.isSelectionEmpty()){
					if(player.isLoaded() && player.isPaused()){
						player.playMusic();
					}else{
						createPlayer(list,label,slider,currentTime);
						setAutor(autor);
						player.playMusic(0);
						System.out.println("slider value : "+slider.getValue()+" slider max: "+slider.getMaximum());
						
					}
				}else{
					System.out.println("nothing selected");
				}
			}
		});
	}
	
	public void setNextSong(JButton button, JList list, JLabel label, JSlider slider, JLabel autor, JLabel currentTime){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!list.isSelectionEmpty()){
					list.setSelectedIndex(list.getSelectedIndex()+1);
					createPlayer(list, label, slider, currentTime);
					setAutor(autor);
					player.playMusic(0);
				}else{
					System.out.println("nothing selected");
				}
			}
		});
	}
	
	public void setPreviousSong(JButton button, JList list, JLabel label, JSlider slider, JLabel autor, JLabel currentTime){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!list.isSelectionEmpty()){
					list.setSelectedIndex(list.getSelectedIndex()-1);
					createPlayer(list, label, slider, currentTime);
					setAutor(autor);
					player.playMusic(0);
				}else{
					System.out.println("nothing selected");
				}
			}
		});
	}
	
	public void createPlayer(JList list,JLabel label, JSlider slider, JLabel currentTime){
		
		if(player.isLoaded()){
			player.stopMusic();
		}
			player = new Playing();
			
			try {	
				player.createPlayer(Cancion.getListaRep().get(list.getSelectedIndex()).getUrl(),slider,currentTime);
							
			} catch (Exception e1) {
				e1.printStackTrace();
				
			}
			
		
			
			
		label.setText(player.getLenght());
	}
	public void setVolumeDown(JMenuItem menuItem, JSlider slider){
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				slider.setValue(slider.getValue()-10);
			}
		});
	}
	public void setVolumeUp(JMenuItem menuitem, JSlider slider){
		
		menuitem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				slider.setValue(slider.getValue()+10);				
			}
		});
		
	}
	
	public void setActionPause(JButton button, JList list){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(player!=null){
					player.pauseMusic();
				}
				
			}
		});
	}
	public void setMenuRefresh(JMenuItem menuItem, JList list){
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				list.setModel(actualizarlista());
			}
		});
	}
	public void setMenuAlbum(JMenuItem menuItem){
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				CreadorAlbum frame = new CreadorAlbum();
				frame.setVisible(true);
			}
		});
	}
	public void setMenuSalir(JMenuItem menuItem, JFrame frame){
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				
			}
		});
	}
	public void setMenuPreferencias(JMenuItem menuItem, JFrame frame){
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Ajustes ajustes = new Ajustes(frame,csc);
				ajustes.setVisible(true);
				ajustes.setIconImage(Toolkit.getDefaultToolkit().getImage(CoffeeSound.class.getResource("/Imagenes/logotipo2.png")));
				ajustes.setTitle("Coffee Sound - Ajustes");
				
			}
		});
	}
	public void setActionRepetir(JButton button){
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (repeticion){
					button.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"repeaticon.png"));
					repeticion = false;
				} else {
					button.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"repeaticonpressed.png"));
					repeticion = true;
				}
			}
		});
	}
	public void setActionAleatorio(JButton button){
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (aleatorio){
					button.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"randomicon.png"));
					aleatorio = false;
				} else {
					button.setIcon(new ImageIcon("src"+conf.getBarra()+conf.getBarra()+"Imagenes"+conf.getBarra()+conf.getBarra()+"randomiconpressed.png"));
					aleatorio = true;
				}
			}
		});
	}
	public void setMenuCancion(JMenuItem menuItem){
		menuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Subir s = new Subir();
				s.setVisible(true);
				
			}
		});
		
	}
	public void setIconImage(JLabel label){
		try {
			client.setFileType(FTP.BINARY_FILE_TYPE);
			File file = new File("user_icon"+Usuario.getCurrentUser().getFoto().substring(Usuario.getCurrentUser().getFoto().lastIndexOf('.'),Usuario.getCurrentUser().getFoto().length()));
			FileOutputStream fos = new FileOutputStream(file);
			client.retrieveFile(Usuario.getCurrentUser().getFoto(), fos);
			fos.close();
			Image i = new ImageIcon(file.getName()).getImage().getScaledInstance(55, 70, Image.SCALE_SMOOTH);
			ImageIcon ii = new ImageIcon(i);
			 
			BufferedImage master = new BufferedImage(ii.getIconWidth(), ii.getIconHeight(), BufferedImage.TYPE_4BYTE_ABGR);
			// or use any other fitting type
			
			master.getGraphics().drawImage(i, 0, 0, null);
		 
		 

		    int diameter = 55;
		    BufferedImage mask = new BufferedImage(master.getWidth(), master.getHeight(), BufferedImage.TYPE_INT_ARGB);

		    Graphics2D g2d = mask.createGraphics();
		    applyQualityRenderingHints(g2d);
		    g2d.fillOval(0, 0, diameter - 1, diameter - 1);
		    g2d.dispose();

		    BufferedImage masked = new BufferedImage(diameter, diameter, BufferedImage.TYPE_INT_ARGB);
		    g2d = masked.createGraphics();
		    applyQualityRenderingHints(g2d);
		    int x = (diameter - master.getWidth(null)) / 2;
		    int y = (diameter - master.getHeight(null)) / 2;
		    g2d.drawImage(master, x, y, null);
		    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.DST_IN));
		    g2d.drawImage(mask, 0, 0, null);
		    g2d.dispose();

			label.setIcon(new ImageIcon(masked));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void applyQualityRenderingHints(Graphics2D g2d) {

	    g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
	    g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

	}
	public void setActionListaMios(JList list, JPanel panelcentral){
		list.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(list.getValueIsAdjusting()){
					Cargando carga = new Cargando();
					Thread thread = null;
					switch(list.getSelectedIndex()){
					case 0:
						//Ventana cargando
						
						carga.setVisible(true);
						
						thread = new Thread(new Runnable() {
							

							@Override
							public void run() {
								Cancion.setListaRep(CancionMetodos.getCancionesUser(Usuario.getCurrentUser()));
								MusicPanel mp = new MusicPanel(panelcentral);
								panelcentral.removeAll();
								panelcentral.add(mp);
								panelcentral.validate();
								carga.dispose();
							}
						});
						thread.start();
						break;
					case 1:
						//Ventana cargando
						
						carga.setVisible(true);
						
						thread = new Thread(new Runnable() {
							

							@Override
							public void run() {
								AlbumPanel ap = new AlbumPanel(AlbumMetodos.getAlbumesUser(Usuario.getCurrentUser()), panelcentral);
								panelcentral.removeAll();
								JScrollPane sp = new JScrollPane();
								sp.setViewportView(ap);
								panelcentral.add(sp);
								panelcentral.validate();
								CoffeeSound.panelColaReproduccion.removeAll();
								JScrollPane sp2 = new JScrollPane();
								sp.setMinimumSize(new Dimension(150,900));
								sp2.setViewportView(CoffeeSound.listaMusic);
								CoffeeSound.panelColaReproduccion.add(sp2);
								CoffeeSound.panelColaReproduccion.validate();
								CoffeeSound.panelColaReproduccion.repaint();
								carga.dispose();
							}
						});
						thread.start();
						break;
					default:
						
					}
					panelcentral.repaint();
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	public void setActionListaNuevo(JList list, JPanel panelcentral){
		list.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(list.getValueIsAdjusting()){
					Cargando carga = new Cargando();
					Thread thread = null;
					switch(list.getSelectedIndex()){
					case 0:
						//Ventana cargando
						
						carga.setVisible(true);
						
						thread = new Thread(new Runnable() {
							

							@Override
							public void run() {
								Cancion.setListaRep(CancionMetodos.getCanciones());
								MusicPanel mp = new MusicPanel(panelcentral);
								panelcentral.removeAll();
								panelcentral.add(mp);
								panelcentral.validate();
								carga.dispose();
							}
						});
						thread.start();
						break;
					case 1:
						//Ventana cargando
						
						carga.setVisible(true);
						
						thread = new Thread(new Runnable() {
							

							@Override
							public void run() {
								AlbumPanel ap = new AlbumPanel(AlbumMetodos.getAlbumes(), panelcentral);
								panelcentral.removeAll();
								JScrollPane sp = new JScrollPane();
								sp.setViewportView(ap);
								panelcentral.add(sp);
								panelcentral.validate();
								CoffeeSound.panelColaReproduccion.removeAll();
								JScrollPane sp2 = new JScrollPane();
								sp.setMinimumSize(new Dimension(150,900));
								sp2.setViewportView(CoffeeSound.listaMusic);
								CoffeeSound.panelColaReproduccion.add(sp2);
								CoffeeSound.panelColaReproduccion.validate();
								CoffeeSound.panelColaReproduccion.repaint();
								carga.dispose();
								
							}
						});
						thread.start();
						break;
					case 2:
						//Ventana cargando
						
						carga.setVisible(true);
						
						thread = new Thread(new Runnable() {
							

							@Override
							public void run() {
								PanelUsers up = new PanelUsers(panelcentral);
								panelcentral.removeAll();
								JScrollPane sp = new JScrollPane();
								sp.setViewportView(up);
								panelcentral.add(sp);
								panelcentral.validate();
								CoffeeSound.panelColaReproduccion.removeAll();
								JScrollPane sp2 = new JScrollPane();
								sp.setMinimumSize(new Dimension(150,900));
								sp2.setViewportView(CoffeeSound.listaMusic);
								CoffeeSound.panelColaReproduccion.add(sp2);
								CoffeeSound.panelColaReproduccion.validate();
								CoffeeSound.panelColaReproduccion.repaint();
								carga.dispose();
							}
						});
						thread.start();
						break;
					default:
						
					}
					panelcentral.repaint();
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	

	
	

	public DefaultListModel actualizarlista(){
		DefaultListModel m = new DefaultListModel();
		m.clear();
		System.out.println("Actualizado");
		try {
			files = client.listFiles();
			
			for(FTPFile file : files){
				if(!file.isDirectory()){
					m.addElement(file.getName());
				}
				
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return m;
		
	}

}
