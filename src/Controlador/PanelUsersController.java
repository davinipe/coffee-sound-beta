package Controlador;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.net.ftp.FTPClient;

import Modelo.AlbumMetodos;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import Objetos.FTPConnection;
import Vista.AlbumPanel;
import Vista.CoffeeSound;
import Vista.UserInfoPanel;

public class PanelUsersController {
	
	FTPClient client = null;
	
	public PanelUsersController() {
		client = FTPConnection.getClient();
	}

	public void setPanelAction(JButton button, JPanel parent, JPanel panelcentral){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				parent.setVisible(false);
				//System.out.println(AlbumMetodos.getAlbumesUser(UsuarioMetodos.getUsuario(button.getText())).get(0).getNombre());
				AlbumPanel ap = new AlbumPanel(AlbumMetodos.getAlbumesUser(UsuarioMetodos.getUsuario(button.getText())),panelcentral);
				panelcentral.removeAll();
				JScrollPane jp = new JScrollPane();
				jp.setViewportView(ap);
				panelcentral.add(jp);
				panelcentral.validate();
				panelcentral.repaint();
				CoffeeSound.panelAutor.removeAll();
				CoffeeSound.panelAutor.add(new UserInfoPanel(UsuarioMetodos.getUsuario(button.getText())));
				
				CoffeeSound.panelAutor.validate();
				CoffeeSound.panelAutor.repaint();
				
			}
		});
	}
	
	public void setIconImage(JLabel label, Usuario user){
		if(user.getFoto().length()>0){
		try {
			File file = new File(user.getFoto().substring(user.getFoto().lastIndexOf('/')+1,user.getFoto().length()));
			if(!file.exists()){
				file.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(file);
			if(client.retrieveFile(user.getFoto(), fos)){
				fos.close();
				Image i = new ImageIcon(file.getName()).getImage().getScaledInstance(55, 70, Image.SCALE_SMOOTH);
				ImageIcon ii = new ImageIcon(i);
				 
				BufferedImage master = new BufferedImage(ii.getIconWidth(), ii.getIconHeight(), BufferedImage.TYPE_4BYTE_ABGR);
				// or use any other fitting type
				
				master.getGraphics().drawImage(i, 0, 0, null);
			 
			 

			    int diameter = 55;
			    BufferedImage mask = new BufferedImage(master.getWidth(), master.getHeight(), BufferedImage.TYPE_INT_ARGB);

			    Graphics2D g2d = mask.createGraphics();
			    applyQualityRenderingHints(g2d);
			    g2d.fillOval(0, 0, diameter - 1, diameter - 1);
			    g2d.dispose();

			    BufferedImage masked = new BufferedImage(diameter, diameter, BufferedImage.TYPE_INT_ARGB);
			    g2d = masked.createGraphics();
			    applyQualityRenderingHints(g2d);
			    int x = (diameter - master.getWidth(null)) / 2;
			    int y = (diameter - master.getHeight(null)) / 2;
			    g2d.drawImage(master, x, y, null);
			    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.DST_IN));
			    g2d.drawImage(mask, 0, 0, null);
			    g2d.dispose();

				label.setIcon(new ImageIcon(masked));
			}else{
				fos.close();
				file.delete();
			}
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
	
	public static void applyQualityRenderingHints(Graphics2D g2d) {

	    g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
	    g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
	    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	    g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

	}
}
