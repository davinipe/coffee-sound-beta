package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.apache.commons.net.ftp.FTPClient;

import Modelo.Album;
import Modelo.Cancion;
import Modelo.CancionMetodos;
import Modelo.Usuario;
import Objetos.FTPConnection;
import Vista.MusicPanel;

public class AlbumPanelController {
	
	FTPClient client;
	public AlbumPanelController() {
		client = FTPConnection.getClient();
	}
	
	public void setActionButton(JButton button, JPanel panelcentral, Album album){
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panelcentral.removeAll();
				Cancion.setListaRep(CancionMetodos.getCancionesAlbum(album));
				
				MusicPanel mp = new MusicPanel(panelcentral);
				panelcentral.add(mp);
				panelcentral.validate();
				panelcentral.repaint();
				
				
			}
		});
	}

}
