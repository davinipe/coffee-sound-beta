package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ReportesController {

	
	public void compruebaReporte(JButton enviar, JTextArea descr, JRadioButton copy, JRadioButton dan, JRadioButton dere, JRadioButton eng, JRadioButton otro) {
		enviar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (copy.isSelected()== false && dan.isSelected()==false && dere.isSelected()==false && eng.isSelected()==false && otro.isSelected()==false ) {
					JOptionPane.showMessageDialog(null, "Debes seleccionar alguna opcion");
				} else {
					JOptionPane.showMessageDialog(null, "Hacer accion de enviar reporte");
				}
			}

		});
	}
	
	public void cerrarVentana(JButton cancel, JFrame ventana){
		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ventana.dispose();
			}

		});
	
	}
}
