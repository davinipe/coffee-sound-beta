package Controlador;

import java.io.IOException;

import javax.swing.DefaultListModel;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import Modelo.Album;
import Modelo.Cancion;
import Modelo.Usuario;
import Modelo.UsuarioMetodos;
import Objetos.FTPConnection;
import Vista.CoffeeSound;

public class MusicPanelController {
	
	FTPClient client = null;
	FTPFile[] files = null;
	
	public MusicPanelController() {
		
	}
	
	public DefaultListModel actualizarlista(){
		DefaultListModel m = new DefaultListModel();
		m.clear();
		
		try {
			
			if(Cancion.getListaRep()!=null && Cancion.getListaRep().size()>0){
				for(Cancion cancion : Cancion.getListaRep()){
						m.addElement(cancion.getNombre());
				}
			}else{
				m.addElement("No hay canciones");
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return m;
		
	}

}
