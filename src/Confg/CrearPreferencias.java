package Confg;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import Idioma.ControlIdioma;
import Temas.ControlTemas;

public class CrearPreferencias {
	public static File f;
	public static Preferencias p= new Preferencias();
	
	public static void CrearPreferencias(){
	String nombreArchivo= "Preferences.txt"; 
	FileWriter fw = null;{
	try { 
	f = new File(nombreArchivo);
	
		fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw); 
		PrintWriter salArch = new PrintWriter(bw); 

		salArch.print(p.getIdioma()); 
		salArch.println(); 
		salArch.print(p.getTema()); 
		salArch.println(); 
		bw.close();
		
	
	} 
	catch (IOException ex) { 
	}
	}
  }
	
	public static void leerPreferncias(){
		String nombreArchivo= "Preferences.txt"; 
		f = new File(nombreArchivo);
			try {
				if(f.exists()){
					FileReader fr = new FileReader(f);
					BufferedReader br = new BufferedReader(fr);
					ControlIdioma.getControlIdioma().setIdioma(br.readLine());
					ControlTemas.getControlTemas().setTema(br.readLine());
					br.close();
					fr.close();
				}
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
	}

}
