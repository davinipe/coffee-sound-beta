package Confg;

import java.io.File;

public class Preferencias {

	String idioma = "ES";
	int nidioma = 0;
	String tema = "neon";
	int nTema = 1;
	String barra = File.separator;
	String nomUsuario;
	int idUsuario;

	public String getNomUsuario() {
		return nomUsuario;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getBarra() {
		return barra;
	}

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}

	public int getNidioma() {
		return nidioma;
	}

	public void setNidioma(int nidioma) {
		this.nidioma = nidioma;
	}

	public int getnTema() {
		return nTema;
	}

	public void setnTema(int nTema) {
		this.nTema = nTema;
	}
	
}