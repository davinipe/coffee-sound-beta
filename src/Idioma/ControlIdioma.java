package Idioma;

import java.io.IOException;
import java.util.Properties;
 
public class ControlIdioma extends Properties{
	public static ControlIdioma ci=null;
	
	private static String idioma="ES";
	
    private ControlIdioma(String idioma){
 
        //Modificar si quieres añadir mas idiomas
        //Cambia el nombre de los ficheros o añade los necesarios
        this.idioma=idioma;
        setPropierties();
 
    }
    
    private void setPropierties(){
    	switch(idioma){
        case "ES":
                getProperties("espanol.properties");
                break;
        case "ENG":
                getProperties("ingles.properties");
                break;
        default:
                getProperties("espanol.properties");
    	}
    }
    
    public void setIdioma(String idioma){
    	this.idioma=idioma;
    	this.setPropierties();
    }
    
    public static ControlIdioma getControlIdioma(){
    	if(ci==null){
    		ci=new ControlIdioma(idioma);
    	}
    	return ci;
    }
    
    private void getProperties(String idioma) {
        try {
            this.load( getClass().getResourceAsStream(idioma) );
        } catch (IOException ex) {
 
        }
   }
}