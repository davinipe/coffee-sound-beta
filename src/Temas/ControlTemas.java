package Temas;

import java.io.IOException;
import java.util.Properties;

	 
public class ControlTemas extends Properties{
	
		public static ControlTemas ci=null;
		private static String tema = "neon";
	
	    private ControlTemas(String thema){
	        this.tema=thema;
	        setPropierties();
	    }
	    
	    private void setPropierties(){
	 
	        //Cambia el nombre de los ficheros o añade los necesarios
	        switch(tema){
	            case "neon":
	                    getProperties("neon.properties");
	                    break;
	            case "minimal":
	                    getProperties("minimal.properties");
	                    break;
	            default:
	                    getProperties("minimal.properties");
	        }
	 
	    }
	    
	    public void setTema(String thema){
	    	this.tema=thema;
	    	this.setPropierties();
	    }
	    
	    public static ControlTemas getControlTemas(){
	    	if(ci==null){
	    		ci=new ControlTemas(tema);
	    	}
	    	return ci;
	    }
	 
	    private void getProperties(String tema) {
	        try {
	            this.load( getClass().getResourceAsStream(tema) );
	        } catch (IOException ex) {
	 
	        }
	   }
}