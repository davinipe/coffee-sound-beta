package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;

public class DB {

		public static DB db = null;
		public Connection con;
		private String server, database, username, password; 
		
		public DB() {
			server="35.162.45.169";
			database = "coffe_sound";
			username = "root";
			password="master";
			
			//Registrem el Diver de Mysql
			try{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
			}catch(Exception e){
				System.err.println("Error registrant el Driver mysql");
			}
			//Connectem amb la nostra base de dades amb l'usuari i contrasenya adequats
			try{
				String cadenaDeConnexio = "jdbc:mysql://"+server+"/"+database+"?user="+username+"&password="+password;
				con= DriverManager.getConnection(cadenaDeConnexio);
			}catch(Exception e){
				System.err.println("Error connectant a mysql " + server + " amb user=root");
			}
		
		}
		
		public static DB getDB(){
			if(db==null){
				db=new DB();
			}
			return db;
		}
		
		


}
