package Modelo;

public class Usuario {

	
	private int id;
	private String nombre;
	private String apellidos;
	private String correo;
	private String password;
	private String username;
	private String foto;
	private int estado;
	private int seguidores;
	private int reportes;
	public static Usuario current_user = null;
	
	public Usuario(int id, String username, String correo, String password, String nombre, String apellidos, String foto, int estado, int seguidores, int reportes) {
		this.id=id;
		this.username=username;
		this.correo=correo;
		this.password=password;
		this.nombre=nombre;
		this.apellidos=apellidos;
		this.estado=estado;
		this.seguidores=seguidores;
		this.reportes=reportes;
		this.foto=foto;
	}
	
	
	public String getFoto() {
		return foto;
	}


	public void setFoto(String foto) {
		this.foto = foto;
	}


	public Usuario(){
		
	}
	
	public static Usuario getCurrentUser(){
		return current_user;
	}
	
	public static void setCurrentUser(Usuario user){
		
		current_user=user;
		
	}
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getSeguidores() {
		return seguidores;
	}

	public void setSeguidores(int seguidores) {
		this.seguidores = seguidores;
	}

	public int getReportes() {
		return reportes;
	}

	public void setReportes(int reportes) {
		this.reportes = reportes;
	}

	

}
