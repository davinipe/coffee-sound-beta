package Modelo;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;

import org.tritonus.share.sampled.file.TAudioFileFormat;

public class CancionMetodos {
	private static DB db = null;
	
	private static void checkDB(){
		if(db==null){
			db=DB.getDB();
		}
	}
	
	public static ArrayList<Cancion> getCanciones(){
		checkDB();
		ArrayList<Cancion> songs = new ArrayList<Cancion>();
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM cancion");
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				Cancion song = new Cancion();
				song.setId(rs.getInt(1));
				song.setNombre(rs.getString(2));
				song.setIdUser(rs.getInt(3));
				song.setAlbum(rs.getInt(4));
				song.setCategoria(rs.getInt(5));
				song.setDuracion(rs.getLong(6));
				song.setUrl(rs.getString(7));
				songs.add(song);
			}
			rs.close();
			return songs;
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		return null;
		}
	}
	
	public static ArrayList<Cancion> getCancionesAlbum(Album user){
		checkDB();
		ArrayList<Cancion> songs = new ArrayList<Cancion>();
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM cancion WHERE `album` = ?");
			stmt.setInt(1, user.getIdalbum());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Cancion song = new Cancion();
				song.setId(rs.getInt(1));
				song.setNombre(rs.getString(2));
				song.setIdUser(rs.getInt(3));
				song.setAlbum(rs.getInt(4));
				song.setCategoria(rs.getInt(5));
				song.setDuracion(rs.getLong(6));
				song.setUrl(rs.getString(7));
				songs.add(song);
			}
			rs.close();
			return songs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static ArrayList<Cancion> getCancionesUser(Usuario user){
		checkDB();
		ArrayList<Cancion> songs = new ArrayList<Cancion>();
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM cancion WHERE `id-user` = ?");
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Cancion song = new Cancion();
				song.setId(rs.getInt(1));
				song.setNombre(rs.getString(2));
				song.setIdUser(rs.getInt(3));
				song.setAlbum(rs.getInt(4));
				song.setCategoria(rs.getInt(5));
				song.setDuracion(rs.getLong(6));
				song.setUrl(rs.getString(7));
				songs.add(song);
			}
			rs.close();
			return songs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static void insertCancion(Cancion song){
		checkDB();
		try {
			PreparedStatement stmt = db.con.prepareStatement("INSERT INTO cancion(`id-user`,nombre,album,categoria,duracion,url) VALUES(?,?,?,?,?,?)");
			stmt.setInt(1, song.getIdUser());
			stmt.setString(2, song.getNombre());
			stmt.setInt(3, song.getAlbum());
			stmt.setInt(4, song.getCategoria());
			stmt.setLong(5, song.getDuracion());
			stmt.setString(6, song.getUrl());
			stmt.execute();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static long getDuracion(File file){
		
		try {
			AudioFileFormat baseFileFormat;
			baseFileFormat = AudioSystem.getAudioFileFormat(file);
			AudioFormat baseFormat = baseFileFormat.getFormat();
		    Map properties = ((TAudioFileFormat)baseFileFormat).properties();
		    String key = "duration";
		    long l =(long) properties.get(key);
		    
		    return l;
		    
		} catch (Exception e) {
			return (Long) null;
		} 
	    
	}

}
