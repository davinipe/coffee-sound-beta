package Modelo;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioMetodos {

	
	private static DB db = null;
	
	private static void checkDB(){
		if(db==null){
			db=DB.getDB();
		}
	}
	
	public static Usuario getUsuario(int iduser){
		checkDB();
		
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM id WHERE id = ?");
			stmt.setInt(1, iduser);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()){
				Usuario user = new Usuario();
				user.setId(rs.getInt(1));
				user.setUsername(rs.getString(2));
				user.setCorreo(rs.getString(3));
				user.setPassword(rs.getString(4));
				user.setNombre(rs.getString(5));
				user.setApellidos(rs.getString(6));
				user.setFoto(rs.getString(7));
				user.setEstado(rs.getInt(8));
				user.setSeguidores(rs.getInt(9));
				user.setReportes(rs.getInt(10));
				return user;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return null;
	}
	public static int getNumeroCanciones(Usuario user){
		checkDB();
		
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT COUNT(id) FROM cancion WHERE `ID-USER` = ?");
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			if(rs.next()){
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		return 0;
		
	}
	
	public static Usuario getUsuario(String username){
		checkDB();
		
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM id WHERE USUARIO = ?");
			stmt.setString(1, username);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()){
				Usuario user = new Usuario();
				user.setId(rs.getInt(1));
				user.setUsername(rs.getString(2));
				user.setCorreo(rs.getString(3));
				user.setPassword(rs.getString(4));
				user.setNombre(rs.getString(5));
				user.setApellidos(rs.getString(6));
				user.setFoto(rs.getString(7));
				user.setEstado(rs.getInt(8));
				user.setSeguidores(rs.getInt(9));
				user.setReportes(rs.getInt(10));
				return user;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return null;
		
	}
	public static ArrayList<Usuario> getUsuarios(){
		checkDB();
		ArrayList<Usuario> users = new ArrayList<Usuario>();
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM id");
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				Usuario user = new Usuario(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getInt(8),rs.getInt(9),rs.getInt(10));
				users.add(user);
			}
			rs.close();
			return users;
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		return null;
		}
	}
	public static String getHexString(byte[] b) throws Exception {
		  String result = "";
		  for (int i=0; i < b.length; i++) {
		    result +=
		          Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
		  }
		  return result;
		}
	public static boolean checkUser(Usuario user){
		checkDB();
		
		
		boolean b=false;
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM id WHERE USUARIO = ? AND CONTRASENA = ?");
			
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getPassword());
			ResultSet rs = stmt.executeQuery();
			
			b = rs.next();
			
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}
	
	public static void setCurrentUser(Usuario user){
		checkDB();
		
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM id WHERE USUARIO = ? AND CONTRASENA = ?");
			
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getPassword());
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()){
				
				Usuario u = new Usuario();
				u.setId(rs.getInt(1));
				u.setUsername(rs.getString(2));
				
				u.setCorreo(rs.getString(3));
				u.setPassword(rs.getString(4));
				u.setNombre(rs.getString(5));
				u.setApellidos(rs.getString(6));
				u.setFoto(rs.getString(7));
				u.setEstado(rs.getInt(8));
				u.setSeguidores(rs.getInt(9));
				u.setReportes(rs.getInt(10));
				Usuario.setCurrentUser(u);
				
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void insertUser(Usuario user){
		checkDB();
		try {
			PreparedStatement stmt = db.con.prepareStatement("INSERT INTO id(USUARIO,CORREO,CONTRASENA,NOMBRE,APELLIDOS,FOTO,ESTADO,SEGUIDORES,REPORTES) VALUES (?,?,SHA1(?),?,?,?,2,0,0)");
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getCorreo());
			stmt.setString(3, user.getPassword());
			stmt.setString(4, user.getNombre());
			stmt.setString(5, user.getApellidos());
			stmt.setString(6, user.getFoto());
			stmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
