package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AlbumMetodos {

	private static DB db = null;
	
	private static void checkDB(){
		if(db==null){
			db=DB.getDB();
		}
	}
	
	public static ArrayList<Album> getAlbumes(){
		checkDB();
		ArrayList<Album> albumes = new ArrayList<Album>();
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM album");
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				Album album = new Album();
				album.setIdalbum(rs.getInt(1));
				album.setIduser(rs.getInt(2));
				album.setNombre(rs.getString(3));
				album.setDescripcion(rs.getString(4));
				album.setFecha(rs.getDate(5));
				album.setImagen(rs.getString(6));
				album.setFavs(rs.getInt(7));
				album.setReportes(rs.getInt(8));
				albumes.add(album);
			}
			rs.close();
			return albumes;
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		return null;
		}
	}
	
	public static ArrayList<Album> getAlbumesUser(Usuario user){
		checkDB();
		ArrayList<Album> albumes = new ArrayList<Album>();
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM album WHERE `id-user` = ?");
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Album album = new Album();
				album.setIdalbum(rs.getInt(1));
				album.setIduser(rs.getInt(2));
				album.setNombre(rs.getString(3));
				album.setDescripcion(rs.getString(4));
				album.setFecha(rs.getDate(5));
				album.setImagen(rs.getString(6));
				album.setFavs(rs.getInt(7));
				album.setReportes(rs.getInt(8));
				albumes.add(album);
			}
			rs.close();
			return albumes;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static void insertAlbum(Album album, int iduser){
		checkDB();
		try {
			PreparedStatement stmt = db.con.prepareStatement("INSERT INTO album(`id-user`,nombre,descripcion,fecha,imagen,favs,reportes) VALUES (?,?,?,NOW(),?,0,0)");
			stmt.setInt(1, iduser);
			stmt.setString(2, album.getNombre());
			stmt.setString(3, album.getDescripcion());
			stmt.setString(4, album.getImagen());			
			stmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
