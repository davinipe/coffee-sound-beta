package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CategoriaMetodos {
	
	private static DB db = null;
	
	private static void checkDB(){
		if(db==null){
			db=DB.getDB();
		}
	}
	
	public static ArrayList<Categoria> getCategorias(){
		checkDB();
		ArrayList<Categoria> categorias = new ArrayList<Categoria>();
		try {
			PreparedStatement stmt = db.con.prepareStatement("SELECT * FROM categoria");
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Categoria categoria = new Categoria();
				categoria.setId(rs.getInt(1));
				categoria.setNombre(rs.getString(2));
				categoria.setDescripcion(rs.getString(3));
				categorias.add(categoria);
			}
			rs.close();
			return categorias;
		} catch (SQLException e) {
			return null;
		}
	}
}
