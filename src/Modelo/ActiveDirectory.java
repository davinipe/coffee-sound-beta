package Modelo;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.swing.JOptionPane;

public class ActiveDirectory {
	

	public static boolean connect(String user, String pass) {


		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.PROVIDER_URL, "ldap://10.2.72.190");
		
		//Rellenamos con el usuario/dominio y password
		env.put(Context.SECURITY_PRINCIPAL, user+"@DAVIRODOMINIO.com");
		env.put(Context.SECURITY_CREDENTIALS, pass);

		DirContext ctx;

		try {
			// Authenticate the logon user
			ctx = new InitialDirContext(env);
			System.out.println("El usuario se ha autenticado correctamente");
			String direccion = "http://35.162.45.169/phpmyadmin";
			try {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + direccion);
			} catch(Exception err) {
				JOptionPane.showMessageDialog(null,"Error: "+err);
			}
			ctx.close();
			return true;

		} catch (NamingException ex) {
			System.out.println("Ha habido un error en la autenticación");
			return false;
		}
			

	}

}
