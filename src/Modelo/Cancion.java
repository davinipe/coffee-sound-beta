package Modelo;

import java.util.ArrayList;

public class Cancion {
	private int id;
	private int idUser;
	private int album;
	private int categoria;
	private String nombre;
	private long duracion;
	private String url;
	
	private static ArrayList<Cancion> cancionesRep = null;
	
	public void Cancion(int id,int idUser,int album,int categoria,String nombre,long duracion,String url){
		this.id=id;
		this.idUser=idUser;
		this.album=album;
		this.categoria=categoria;
		this.nombre=nombre;
		this.duracion=duracion;
		this.url=url;
	}

	
	public static ArrayList<Cancion> getListaRep(){
		return cancionesRep;
	}
	
	public static void setListaRep(ArrayList<Cancion> canciones){
		cancionesRep=canciones;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getAlbum() {
		return album;
	}

	public void setAlbum(int album) {
		this.album = album;
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getDuracion() {
		return duracion;
	}

	public void setDuracion(long duracion) {
		this.duracion = duracion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
