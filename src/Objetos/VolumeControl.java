package Objetos;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Port;
import javax.sound.sampled.Line.Info;

public class VolumeControl {
	public static float volume = 0.5f;
	public FloatControl volumeControl = null;
	public static VolumeControl vc = null;
	
	private VolumeControl() {
		Info source = Port.Info.SPEAKER;
		try {
			Port outline = (Port) AudioSystem.getLine(source);
			outline.open();                
			volumeControl = (FloatControl) outline.getControl(FloatControl.Type.VOLUME);                
			
		} catch (LineUnavailableException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static VolumeControl getVC(){
		if(vc == null){
			vc = new VolumeControl();
		}
		return vc;
	}
	
	public static void setVolume(float vol){
		volume=vol;
	}

}
