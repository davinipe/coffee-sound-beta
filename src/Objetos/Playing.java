package Objetos;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;
import javax.swing.JSlider;

import org.apache.commons.net.ftp.FTPClient;

import javazoom.jl.decoder.JavaLayerException;

public class Playing {


	private AdvancedPlayer player = null;
	private File f= null;
	private FTPClient client = null;
	private JSlider slider = null;
	private JLabel time = null;
	private long duration;
	
	public Playing(){
		
	}
	
	public void createPlayer(String file, JSlider slider, JLabel currentTime){
		try {
			
			this.time=currentTime;
			this.slider=slider;
			
			createTemp(file);
			createAdvancedPlayer();
			slider.getModel().setMaximum((int) getFrames());
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error al crear Playing");
		}
	}
	
	public void createTemp(String file) throws Exception{
		
		f=new File("temp.mp3");
		this.client=FTPConnection.getClient();
		client.changeWorkingDirectory(file.substring(0,file.lastIndexOf("/")));
		System.out.println(client.printWorkingDirectory());
        OutputStream outputStream = new BufferedOutputStream(
                new FileOutputStream(f));
        boolean success = client.retrieveFile(file.substring(file.lastIndexOf("/")+1,file.length()), outputStream);
        outputStream.close();
        if(success){
        	Temp.getTemp().setFile(f);
        }
        client.changeWorkingDirectory("/");
	}
	public void createAdvancedPlayer(){
		
		try {
			this.player=new AdvancedPlayer(new FileInputStream(f));
			
		} catch (FileNotFoundException | JavaLayerException e) {
			// TODO Auto-generated catch block
			System.out.println("error al crear el AdvancedPlayer");
		}
	}
	
	public long getFrames(){
		Map properties;
		try {
			properties = Temp.getTemp().getProperties();
			String key = "mp3.length.frames";
		    long l = (int) properties.get(key);
			return l;
		} catch (Exception e) {
			return 0;
		}
		
	}
	public boolean isLoaded(){
		if(this.player!=null){
			return true;
		}else{
			return false;
		}
	}
	public boolean isPaused(){
		return !this.player.playing;
	}
	public String getLenght(){

			Map properties = null;
			try {
				properties = Temp.getTemp().getProperties();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    //System.out.println(val);
		    String key = "duration";
		    Long l =(Long) properties.get(key);
		    this.duration = TimeUnit.MICROSECONDS.toSeconds(l);
		    String s = String.format("%02d:%02d", 
		    	    TimeUnit.MICROSECONDS.toMinutes(l),
		    	    TimeUnit.MICROSECONDS.toSeconds(l) - 
		    	    TimeUnit.MINUTES.toSeconds(TimeUnit.MICROSECONDS.toMinutes(l))
		    	);
		    return s;
	}
	
	
	public void playMusic(){
		try {
			if(!player.playing){
				player.resume();
			}else{
				getLenght();
				this.player.play(getFrames(),this.slider,this.time,this.duration);
			}
			
		} catch (JavaLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(getFrames());
	}
	public void playMusic(long init){
		try {
			if(!player.playing){
				player.resume();
			}else{
				getLenght();
				this.player.play(init,getFrames(),this.slider,this.time,this.duration);
			}
			
		} catch (JavaLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(getFrames());
	}
	
	public void stopMusic(){
		if(player!=null){
			this.player.stop();
			this.player=null;
		}
		
	}
	
	public void pauseMusic(){
		if(player!=null){
			this.player.pause();
		}
		
	}
	
	
	
	
}
