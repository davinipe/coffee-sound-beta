package Objetos;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.Line.Info;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Port;
import javax.swing.JLabel;
import javax.swing.JSlider;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.decoder.SampleBuffer;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.FactoryRegistry;



public class AdvancedPlayer
{
  private InputStream is;
  private Bitstream bitstream;
  private Decoder decoder;
  private AudioDevice audio;
  private boolean closed = false;
  public boolean complete = false;
  private int lastPosition = 0;
  private PlaybackListener listener;
  public boolean playing = true;
  private boolean stop = false;
  private boolean INIT = true;
  private Header h = null;
  private long init = 0;
  private Long max = 0l;
  private JSlider slider;
  
  public AdvancedPlayer(InputStream paramInputStream)
    throws JavaLayerException
  {
    this(paramInputStream, null);
  }
  
  public AdvancedPlayer(InputStream paramInputStream, AudioDevice paramAudioDevice)
    throws JavaLayerException
  {
	this.is=paramInputStream;
    bitstream = new Bitstream(paramInputStream);
    if (paramAudioDevice != null) {
      audio = paramAudioDevice;
    } else {
      audio = FactoryRegistry.systemRegistry().createAudioDevice();
    }
    audio.open(this.decoder = new Decoder());
  }
  
  public boolean play(long paramInt,JSlider slider, JLabel time, long max)
    throws JavaLayerException
  {
	//System.out.println(max);
	
	init = paramInt;
    this.slider=slider;
    long initialTime = System.currentTimeMillis();
//    this.slider.setMaximum(init);
    if (listener != null) {
      listener.playbackStarted(createEvent(PlaybackEvent.STARTED));
    }
    
    Thread t = new Thread(new Runnable() {
		
		@Override
		public void run() {
			Info source = Port.Info.SPEAKER;
			FloatControl fc = null;
			try {
				fc = VolumeControl.getVC().volumeControl;
				Port outline = (Port) AudioSystem.getLine(source);
				outline.open();
				
			} catch (LineUnavailableException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}  
			boolean bool = true;
			long paramInt = init;
			
		    while ((paramInt > 0) && (bool)) {
		    	fc.setValue(VolumeControl.volume);
		    	
		    	try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
		    	
		    	if(playing){
		    		slider.setValue((int) (slider.getMaximum()-paramInt));
		    		long actualTime = (slider.getValue()*max)/slider.getMaximum();
		    		time.setText(String.format("%02d:%02d", 
				    	    TimeUnit.SECONDS.toMinutes(actualTime),
				    	    TimeUnit.SECONDS.toSeconds(actualTime) - 
				    	    TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(actualTime))
				    	));
		    		try {
						bool = decodeFrame();
					} catch (JavaLayerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		
		    		paramInt--;
		    		
		    	}else if(stop){
		    		bool=false;
		    	}
//		    	System.out.println(paramInt);
		    	
		    
		    	
//		    	System.out.println(paramInt);
		    	
		      
		    }
			
		}
	});
    t.start();
    return false;
//    AudioDevice localAudioDevice = audio;
//    if (localAudioDevice != null && playing)
//    {
//      localAudioDevice.flush();
//      
//      synchronized (this)
//      {
//    		  complete = (!closed);
//    	        close();
//      }
//      if (listener != null) {
//        listener.playbackStarted(createEvent(localAudioDevice, PlaybackEvent.STOPPED));
//      }
//    }
//    return bool;
  }
  
  public synchronized void close()
  {
    AudioDevice localAudioDevice = audio;
    if (localAudioDevice != null)
    {
      closed = true;
      audio = null;
      localAudioDevice.close();
      lastPosition = localAudioDevice.getPosition();
      try
      {
        bitstream.close();
      }
      catch (BitstreamException localBitstreamException) {}
    }
  }
  
  protected boolean decodeFrame()
    throws JavaLayerException
  {
    try
    {
      AudioDevice localAudioDevice = audio;
      if (localAudioDevice == null) {
        return false;
      }
      Header localHeader = bitstream.readFrame();
      if(INIT){
    	  this.h=localHeader;
    	  INIT=false;
      }
      
      if (localHeader == null) {
        return false;
      }
      
      SampleBuffer localSampleBuffer = (SampleBuffer)decoder.decodeFrame(localHeader, bitstream);
      synchronized (this)
      {
        localAudioDevice = audio;
        if (localAudioDevice != null) {
//        	System.out.println("Buffer: "+localSampleBuffer.getBuffer().length);
        	localAudioDevice.write(localSampleBuffer.getBuffer(), 0, localSampleBuffer.getBufferLength());
        }
      }
      bitstream.closeFrame();
    }
    catch (RuntimeException localRuntimeException)
    {
      throw new JavaLayerException("Exception decoding audio frame", localRuntimeException);
    }
    return true;
  }
  
  protected boolean skipFrame()
    throws JavaLayerException
  {
    Header localHeader = bitstream.readFrame();
    if (localHeader == null) {
      return false;
    }
    bitstream.closeFrame();
    return true;
  }
  
  public boolean play(Long long1, Long long2, JSlider slider, JLabel time, long max)
    throws JavaLayerException
  {
	  this.slider=slider;
	  this.max=long2;
    boolean bool = true;
    Long i = long1;
    while ((i-- > 0) && (bool)) {
      bool = skipFrame();
    }
    return play((int) (long2 - long1),slider,time,max);
  }
  
  private PlaybackEvent createEvent(int paramInt)
  {
    return createEvent(audio, paramInt);
  }
  
  private PlaybackEvent createEvent(AudioDevice paramAudioDevice, int paramInt)
  {
    return new PlaybackEvent(this,paramInt,paramAudioDevice.getPosition());
  }
  
  public void setPlayBackListener(PlaybackListener paramPlaybackListener)
  {
    listener = paramPlaybackListener;
  }
  
  public PlaybackListener getPlayBackListener()
  {
    return listener;
  }
  
  public void stop()
  {
	this.playing = false;
	this.stop=true;
	
    //listener.playbackFinished(createEvent(PlaybackEvent.STOPPED));
    //close();
  }
  
  public void pause(){
	  
	  this.playing=false;
//	  System.out.println(init+" paused");
  }
  
  public void resume()
  {
	  this.playing=true;
	  this.stop=false;
//	  System.out.println(init+" resumed");
	  
  }
}
