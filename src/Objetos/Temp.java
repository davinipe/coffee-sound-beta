package Objetos;

import java.io.File;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;

import org.tritonus.share.sampled.file.TAudioFileFormat;

public class Temp {
	
	private File file = null;
	public static Temp temp = null;
	
	private Temp(){
		
	}
	public Map getProperties() throws Exception{
		AudioFileFormat baseFileFormat;
		try {
			baseFileFormat = AudioSystem.getAudioFileFormat(this.file);
		} catch (Exception e) {
			return null;
		} 
		AudioFormat baseFormat = baseFileFormat.getFormat();
		
		
	    Map properties = ((TAudioFileFormat)baseFileFormat).properties();
	    
	    return properties;
	
			
		
		
		
	}
	public void setFile(File f){
		this.file = f;
	}
	
	public File getFile(){
		return this.file;
	}
	
	public static Temp getTemp(){
		if(temp==null){
			temp=new Temp();
		}
		return temp;
	}
	
	
}
