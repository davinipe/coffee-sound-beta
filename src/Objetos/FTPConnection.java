package Objetos;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Types;
import java.util.Scanner;

import javax.print.DocFlavor.URL;
import javax.sound.sampled.AudioFileFormat.Type;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;


public class FTPConnection {
	public static FTPClient client = null;
	private static int pausedOnFrame = 0;
    private static void showServerReply(FTPClient ftpClient) {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                System.out.println("SERVER: " + aReply);
            }
        }
    }
    public static FTPClient getClient(){
    	
    	if(client==null){
    		createConnection();
    	}
    	
    	return client;
    }
    private static void createConnection() {

        String server = "127.0.0.1";
        int port = 21;
        String user = "remote";
        String pass = "remote";
//        String server = "35.162.45.169";
//        int port = 21;
//        String user = "java";
//        String pass = "java";

        client = new FTPClient();
        if(client.getClass().getClassLoader() instanceof java.net.URLClassLoader) {
            java.net.URL[] urls = ((java.net.URLClassLoader) client.getClass().getClassLoader()).getURLs();
            // log these urls somewhere and check - these are urls from where your FTPClient may be loaded from
        }
        try {
            client.connect(server, port);
            
            client.enterLocalPassiveMode();
           
            showServerReply(client);
            int replyCode = client.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                System.out.println("Operation failed. Server reply code: " + replyCode);
                
            }
            boolean success = client.login(user, pass);
            showServerReply(client);
            if (!success) {
                System.out.println("Could not login to the server");
               
            } else {
                System.out.println("LOGGED IN SERVER");
            }
            
            
           
			
        } catch (IOException ex) {
            System.out.println("Oops! Something wrong happened");
            ex.printStackTrace();
        }
       
    }
    
}